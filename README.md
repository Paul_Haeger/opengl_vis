# Open GL Visualizer

**THIS IS STILL WIP AND UNFINISHED**

This project will make it possible to render out custom OpenGL shaders you wrote.
This is done over a XML file which allows you to define the shaders to be used, as well as different inputs for these shaders.

## License
- This project is licensed under the GPL v3 license. For more information see LICENSE.
- The stb_image.h header is licensed under the MIT license.
- The glad.h is licensed under Public Domain.
 -The KHR/khrplatform.h is either licensed under the Apache Version 2.0 license or Public Domain (not that clear in the moment, but it does not matter for this project as both is GPL v3 compatible).
- The glad.c is licensed under the Apache Version 2.0 license.


## Basic feature set

This are the basic features I will try to implement for the first release.

- Configurable over a XML file
- Define custom fragment shaders
- Ability to feed the FFT of an audio file to a shader
- Ability to feed an image to a shader
- Ability to feed an image sequence to a shader
- Ability to feed internal data such as time or resolution to the shader
- Reading from a custom framebuffer
- Writing to a custom framebuffer instead of the standard one
- A real-time mode that allows you to see your shaders in action
    - With custom resolution
    - Ability to reload shaders
    - Ability to reload the configuration
- A render mode which will be able to render out your shaders
    - Currently only PNG image sequence as possibility planned
- Framebuffers that are persistent between draw calls

## Addition features

These are features that I would personally like to see. This is not a confirmation that I will be working on these features.
Treat this more of a backlog of possibilities. This list is not ordered in some way.

- A GUI(very hard)
- XML defined shader parameters which are fed to an uniform
- Automations 
- Waveform input for audio
- Lookahead option for the FFT input(you can hack it in the moment by just setting the offset)
- Video file inclusion
- Constant framebuffer that is only drawn once
- Output as video files
    - Allow to selectively mute audio tracks in the XML file

## Documentation

A lot still has to be done. I have to get doxygen to properly work with my codebase and not everything is completely documented as of yet. Still WIP.


