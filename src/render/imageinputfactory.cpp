/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/imageinputfactory.h"
#include "render/imageinput.h"
#include "render/input.h"
#include "config/imageinput.h"
#include "config/inputbase.h"
#include <memory>

namespace render 
{
	/*virtual*/ std::shared_ptr<render::Input> ImageInputFactory::constructInput(RenderContext* parent, const config::Input* input)
	{
		if(input->getType() != config::InputType::Image){
			throw SetupException("Given configuration is not of the type 'Image'");	
		}	
		const config::ImageInput* imageInput = static_cast<const config::ImageInput*>(input);

		return std::shared_ptr<Input>(new ImageInput(
				parent,
				imageInput->getPath(),
				imageInput->getWrap(),
				imageInput->getFilering()			
		));

		return std::make_shared<ImageInput>(
				parent, 
				imageInput->getPath(), 
				imageInput->getWrap(), 
				imageInput->getFilering()
		);
	}
}

