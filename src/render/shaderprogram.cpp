/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/shaderprogram.h"
#include "config/shaderprogram.h"
#include "render/rendercontext.h"
#include "gl/shader.h"
#include <fstream>
#include <regex>

const char* defaultVertex = "layout (location = 0) in vec3 vert_pos; // the position variable has attribute position 0 \n  \
void main() \n \
{ \n  \
    gl_Position = vec4(vert_pos, 1.0); \n \
}"; 


namespace render
{
		/*static*/ std::string ShaderProgram::generateVertexShader(const std::string& path){
				std::ifstream f;
				f.open(path, std::ios::in);
				std::string header;
				if(f.is_open()){
						std::string tmp;
						std::regex versionFinder("^\\s*#version [\\d\\s\\w\\.]+\\s*$"); //finds the #version number. Missmatching versions do seem to pass in tests. /TODO: Find out if necessary
						bool bVersionFound = false;
						while(std::getline(f,tmp)){
							if(!bVersionFound){
								if(std::regex_match(tmp, versionFinder)){
										bVersionFound = true;
										header += tmp + '\n';
										continue;
								}
							}
						}
						f.close();
						//found all uniforms hopefully
						std::string src = header + defaultVertex;
						return src;
				}else{
					throw SetupException("GenerateVertexShader:Failed to open path:'" + path + "'" );		
				}
		}


		void ShaderProgram::applyShadersProgram(const config::ShaderProgram& data){
				gl::Shader vertex, fragment;
				fragment.fromFile(data.getFragmentPath(), gl::Shader::Fragment);
				fragment.compile();
				std::string src = generateVertexShader(data.getFragmentPath());
				vertex.fromCode(src, gl::Shader::Vertex);
				vertex.compile();
				program.attachShader(vertex);
				program.attachShader(fragment);

				vertex.free();
				fragment.free();

				//Shaders attached	
				const std::vector<config::UniformData>& uniformData = data.getUniformsConst();
				for(const config::UniformData& var : uniformData){
					try{
						std::shared_ptr<gl::Uniform> ptr(new gl::Uniform(program, var.internalName));
						
						if(var.isCustom){
							parent->addUniform(var.id, ptr);					
			
						}else{
							parent->addUniform(var.type, ptr);
						}
					} catch(const gl::UniformException& e){
						throw SetupException("Failed to bind uniform '" + var.internalName +
						   		"'. Make sure the uniform is used in the code, because \
else it is removed in the compilation process!");
					}
				}
				//Uniforms attached
				//should be finished now
		}

		void ShaderProgram::use() const {
			program.useProgram();
		}
} // namespace render

