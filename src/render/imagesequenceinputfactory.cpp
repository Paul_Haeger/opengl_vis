/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/imagesequenceinputfactory.h"
#include "render/imagesequenceinput.h"
#include "config/inputbase.h"
#include "config/imagesequenceinput.h"
#include <memory>

namespace render
{

	/*virtual*/ std::shared_ptr<Input> render::ImageSequenceInputFactory::constructInput(RenderContext* parent, const config::Input* input)
	{
		if(input->getType() != config::InputType::ImageSequence){
				throw SetupException("Given configuration is not of the type 'ImageSequence'");
		}
		const config::ImageSequenceInput* imageInput = static_cast<const config::ImageSequenceInput*>(input);

		//Extract the path with filename and type, while ignoring the frame number if any is given.
		//Still not perfect, maybe its possible to do a more robust version in the future.
		std::string tmp = imageInput->getPath();
		std::string rest = tmp;
		std::string fileEnding;
		size_t dotPos =tmp.find_last_of('.');
		if (dotPos != std::string::npos) {
			fileEnding = tmp.substr(dotPos);
			rest = tmp.substr(0,dotPos);
		}
		//remove the trailing digits if any are given
		std::string::const_iterator pos = rest.cend();
		for(auto it = rest.cend() - 1; it != rest.cbegin(); --it){
				if(std::isdigit(*it)){
				pos = it;
			}else{
				break;
			};			
		}
		if(pos != rest.cend()){
			rest.erase(pos, rest.cend());
		}

		return std::shared_ptr<Input>(new ImageSequenceInput(
				parent, 
				imageInput->getStart(),
				imageInput->getEnd(),
				imageInput->getFrameRate(),
				imageInput->isLooped(),
				rest,
				fileEnding,
				imageInput->getFormatString(),
				imageInput->getWrap(),
				imageInput->getFilering()
		));
	}

		
	
} // namespace render
