/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/audioinputfactory.h"
#include "render/rendercontext.h"
#include "render/audioinput.h"
#include "config/audioinput.h"
#include <memory>

namespace render
{
	

/*virtual*/ std::shared_ptr<Input> AudioInputFactory::constructInput(RenderContext* parent, const config::Input* input) {
		if(input->getType() == config::InputType::Audio){
			const config::AudioInput* inputData = static_cast<const config::AudioInput*>(input);

			return std::shared_ptr<Input>(new AudioInput(
					parent, 
					inputData->isLooped(),
					0.0,
					inputData->getOffset(),	
					inputData->getSkip(),
					inputData->getSamplingRate(),
					inputData->getWindowSize(),
					inputData->getFttTrail(),
					inputData->getPath()
			));
		}else {
			throw SetupException("Input of the wrong type given, AudioInput expected");
		}

}

} // namespace render
