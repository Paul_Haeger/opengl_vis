/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/inputdirector.h"
#include "config/inputbase.h"

#include "render/input.h"
#include "render/inputfactory.h"
#include "render/imageinputfactory.h"
#include "render/imagesequenceinputfactory.h"
#include "render/audioinputfactory.h"

#include <memory>

namespace render
{

	/*static*/ std::unique_ptr<render::InputFactory> render::InputDirector::getFactory(const config::Input& input)
	{
		switch (input.getType()) {
			case config::InputType::Audio:
				return std::unique_ptr<render::InputFactory>(new AudioInputFactory);
				break;
			case config::InputType::Image:
				return std::unique_ptr<render::InputFactory>(new ImageInputFactory());	
			case config::InputType::ImageSequence:
				return std::unique_ptr<render::InputFactory>(new ImageSequenceInputFactory);
			case config::InputType::FrameBuffer:
				//TODO
				break;	
			default:
				throw SetupException("Unkown type for input '" + input.getInternalName() + "'");	
		}
		return std::unique_ptr<InputFactory>(nullptr);
	}
}
