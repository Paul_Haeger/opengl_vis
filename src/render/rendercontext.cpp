/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/rendercontext.h"

namespace render
{

	RenderContext::RenderContext(unsigned int inFrameRate, unsigned int inFrameCount, const config::Resolution_t& res)
		: frameRate(inFrameRate), frameCount(inFrameCount), resolution(res)
	{
	}

	void RenderContext::implementConfiguration(const XmlConfig& config){
			//TODO
	}		

	void RenderContext::addUniform(const std::string& id, std::shared_ptr<gl::Uniform> uniform){
			//uniforms.emplace(id, uniform);
			if(customDataUniforms.find(id) == customDataUniforms.end()){
				//key does not exists, create key value pair
				customDataUniforms.emplace(id, std::deque<std::shared_ptr<gl::Uniform>>());
			}
			customDataUniforms[id].push_back(uniform);
	}

	void RenderContext::addUniform(config::DefaultType type, std::shared_ptr<gl::Uniform> uniform){
			if(defaultDataUniforms.find(type) == defaultDataUniforms.end()){
				defaultDataUniforms.emplace(type, std::deque<std::shared_ptr<gl::Uniform>>());
			}
			defaultDataUniforms[type].push_back(uniform);
	}

} // namespace render
