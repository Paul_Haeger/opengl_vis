/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/framebufferinput.h"
#include "gl/framebuffer.h"
#include "gl/texture.h"


namespace render
{
	void FramebufferInput::swapBuffers(){
		if(bIsBeingRenderedTo){
			//Make sure we only swap if we are being written to.
			currentBuffer ^= 1;
			buffers[currentBuffer].bind();
		}
	}

	FramebufferInput::FramebufferInput(RenderContext* inParent,  config::Resolution_t res)
		: Input(inParent)
	{
		textures[0].bind();
		textures[1].bind();
		
		for (size_t i = 0; i < textures.size(); ++i) {
			textures[i].bind();	
			gl::Texture::TexImage2D(gl::Texture::Texture2D, 0, GL_RGBA, static_cast<GLsizei>(res.first),static_cast<GLsizei>(res.second), 0, GL_RGBA, GL_FLOAT, NULL );
			buffers[i].bind();
			gl::FrameBuffer::FramebufferTexture2D(gl::FrameBuffer::Target::FrameBufferBoth, GL_COLOR_ATTACHMENT0, textures[i].getType(), textures[i], 0);
		}
		

	}

	void FramebufferInput::advance(double deltaTime){
//		currentBuffer ^= 1; //simple flip flop
	}

	/*virtual*/ void FramebufferInput::prepareForRender(gl::Uniform& u)
	{
		swapBuffers(); //Swap the render and the read buffer so we can read the result of the last draw call.
		unsigned char textureBuffer = currentBuffer ^ 1;//Take the other buffer
		textures[textureBuffer].bind();
		GLenum id = gl::Texture::ActiveTextureAtNextFreeUnit();
		u.setValue(static_cast<GLint>(id));

		
	}

	/*virtual*/ void FramebufferInput::bindRenderTarget()
	{
		buffers[currentBuffer].bind();
		bIsBeingRenderedTo = true;
	}

	/*virtual*/ void FramebufferInput::unbindRenderTarget()
	{
		bIsBeingRenderedTo = false;
	}


	/*virtual*/ gl::FrameBuffer& FramebufferInput::getTargetFrameBuffer()
	{
		return buffers[currentBuffer];
	}

	/*virtual*/ gl::Texture& FramebufferInput::getTargetTexture()
	{
		return textures[currentBuffer];
	}

} // namespace render 

