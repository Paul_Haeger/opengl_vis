/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/rendercontextbuilder.h"
#include "render/rendercontext.h"
#include "config/config.h"
#include <memory>

namespace  render
{
	/*static*/ std::shared_ptr<RenderContext> RenderContextBuilder::constructContext(const config::Config& config)
	{
		std::shared_ptr<RenderContext> context = std::make_shared<RenderContext>(
				config.getFrameRate(), 
				config.getFrameDuration(), 
				config.getResolution()
				);	

		auto inputs = config.getInputs();	
		for(auto it = inputs.cbegin() ; it != inputs.cend(); ++it){

		}



		return context;
	}

}

