/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/imagesequenceinput.h"
#include <cstdio>
#include <regex>
#include "config/imagesequenceinput.h"

namespace render
{
		
	ImageSequenceInput::ImageSequenceInput(RenderContext* inParent, unsigned int inStart, unsigned int inEnd,
					unsigned int inFrameRate, bool inBIsLooped, std::string inPath, 
					std::string inFileEnding, std::string inFormatString,
					gl::Texture::Wrapping wrap, gl::Texture::Filtering filtering) 
			: ImageInput(inParent,  wrap, filtering),  start(inStart), end(inEnd), frameRate(inFrameRate),
	   	bIsLooped(inBIsLooped), path(inPath),  fileEnding(inFileEnding), formatString(inFormatString)
			{
				
			}
	

	void ImageSequenceInput::advance(double time) {
		//how many frames did we advance
		//Could be optimized by stopping calculating the frame after the end is reached.
		elapsedTime += time;
		unsigned int frame =  static_cast<unsigned int>(elapsedTime * frameRate);
		if(bIsLooped){
			frame = frame % (end - start);
		}else{
			//Clip the frame if we are at the end
			frame = frame + start > end ? end : frame;
		}
		currentFrame = frame + start;
		
		//Load the new texture

		char buff[12]; //hopefully 12 digits are enough, not sure why youd need that many frames.
		int read = snprintf(buff, sizeof(buff), formatString.c_str(), currentFrame);
		if(read <= 0){
			throw RenderException("Failed to output numbers. The framenumber might be \
					larger than 12 digits or an internal error happened.");
		}
		std::string frameFormatted  = buff;
		
		loadTexture(path + frameFormatted + fileEnding);
	}

	void ImageSequenceInput::prepareForRender(gl::Uniform& u){
			tex.bind(); //Assuming its not bound already
			GLuint unit = gl::Texture::ActiveTextureAtNextFreeUnit();
			u.setValue(static_cast<GLint>(unit));
	}

	void ImageSequenceInput::implementConfiguration(const config::Input* input) {
		if(input->getType() != config::InputType::ImageSequence){
				throw SetupException("Given configuration is not of the type 'ImageSequence'");
		}
		const config::ImageSequenceInput* imageInput = static_cast<const config::ImageSequenceInput*>(input);

		formatString = 	imageInput->getFormatString();
		frameRate = imageInput->getFrameRate();
		bIsLooped = imageInput->isLooped();
		start = imageInput->getStart();
		end = imageInput->getEnd();
		frameRate =  imageInput->getFrameRate();
		tex.setWrap(imageInput->getWrap());
		tex.setFiltering(imageInput->getFilering());
		
		//Extract the path with filename and type, while ignoring the frame number if any is given.
		//Still not perfect, maybe its possible to do a more robust version in the future.
		std::string tmp = imageInput->getPath();
		std::string rest = tmp;
		size_t dotPos =tmp.find_last_of('.');
		if (dotPos != std::string::npos) {
			fileEnding = tmp.substr(dotPos);
			rest = tmp.substr(0,dotPos);
		}
		//remove the trailing digits if any are given
		std::string::const_iterator pos = rest.cend();
		for(auto it = rest.cend() - 1; it != rest.cbegin(); --it){
				if(std::isdigit(*it)){
				pos = it;
			}else{
				break;
			};			
		}
		if(pos != rest.cend()){
			rest.erase(pos, rest.cend());
		}
		path = rest;	
			
	}

} // namespace render
