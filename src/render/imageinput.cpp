/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/imageinput.h"
#include "config/imageinput.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "gl/uniform.h"
#include "gl/texture.h"

namespace render
{
	void ImageInput::loadTexture(const std::string& path){
		stbi_set_flip_vertically_on_load(true);
		data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);
		if(data == nullptr){
			throw SetupException("Failed to open image file '" + path + "'.");
		}
		bIsLoaded = true;
		tex.bind();
		gl::Texture::TexImage2D(gl::Texture::Texture2D,  0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);	
		gl::Texture::GenerateMipmaps(gl::Texture::Texture2D);	
		stbi_image_free(data);
		bIsLoaded = false;	
	}

	ImageInput::ImageInput(RenderContext* inParent, gl::Texture::Wrapping wrap, gl::Texture::Filtering filter)
		: Input(inParent)
	{
		tex.setWrap(wrap);
		tex.setFiltering(filter);
	}

	ImageInput::ImageInput(RenderContext* inParent, const std::string& imagePath,
		   	gl::Texture::Wrapping wrap, gl::Texture::Filtering filtering)
		: Input(inParent) {
			tex.setWrap(wrap);
			tex.setFiltering(filtering);	
			loadTexture(imagePath);

	};

	ImageInput::~ImageInput(){
		if(bIsLoaded){
			stbi_image_free(data);
		}
	}

 	void ImageInput::advance(double time) {}

	void ImageInput::prepareForRender(gl::Uniform& u){
		tex.bind();
		GLenum pos = gl::Texture::ActiveTextureAtNextFreeUnit();
		u.setValue(static_cast<GLint>(pos)); //explicit cast to make sure we are actually using the int method.	
	}


} // namespace render

