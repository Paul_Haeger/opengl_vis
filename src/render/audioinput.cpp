/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "render/audioinput.h"
#include "config/audioinput.h"
#include <cstdio>
#include <memory>
#define _USE_MATH_DEFINES //For older systems
#include <math.h>

namespace render
{
	void AudioInput::_calculateFFTData(){
		float* ptr = textureData.data();
		//move previous data back
		memmove(ptr + windowToBinSize(windowSize), ptr, textureData.size() - windowToBinSize(windowSize));
		//calculate new fftdata
		//Apply hammingwindow
		constexpr float fac = 0.53836f *(1.f- 0.53836f);//Hamming factor	
		for(unsigned int i = 0; i < windowSize; ++i){
			samples[i] *= fac * std::cos(2.f* static_cast<float>(M_PI) * (static_cast<float>(i)/ static_cast<float>(windowSize)));
		}
		if(complexPoints.size() < windowToBinSize(windowSize)){
			complexPoints.resize(windowToBinSize(windowSize));
		}
		//the fft has n samples in and returns n/2+1 bins
		kiss_fftr(*cfg, samples.data(), complexPoints.data());
		for(size_t i = 0 ; i < complexPoints.size(); ++i){
			const kiss_fft_cpx& p = complexPoints[i];
			textureData[i] = std::sqrt(p.i * p.i + p.r * p.r);
		}
						
		//Everything calculated	

	}
	
	void AudioInput::FFTDeleter::operator()(kiss_fftr_cfg* toDelete) const {
		kiss_fftr_free(*toDelete);
		delete toDelete;
	}	

	AudioInput::AudioInput(
			RenderContext* 
			inParent, 
			bool inBIsLooped ,
			double inOffset, 
			double inElapsedTime, 
			double inSkip,  
			unsigned int inSamplingRate, 
			unsigned int inWindowSize,
			unsigned int fftTrail,
		   	const std::string& file )
			
		:Input(inParent), bIsLooped(inBIsLooped), elapsedTime(inElapsedTime-inOffset), skip(inSkip), samplingRate(inSamplingRate), windowSize(inWindowSize)  {

			reader = std::make_unique<AudioReader>(file, samplingRate);		
			*cfg = kiss_fftr_alloc(static_cast<int>(windowSize),0 , nullptr, nullptr);
			
			textureData = std::vector<float>(fftTrail * windowToBinSize(windowSize) ,0.0f); //Initialize the array.
			
			//Initalize the empty texture in case there is an offset
			tex.bind();
			tex.TexImage2D(
					gl::Texture::Texture2D, 
					0, 
					GL_R32F, 
					static_cast<GLsizei>(windowToBinSize(windowSize)), 
					static_cast<GLsizei>(textureData.size() / (windowToBinSize(windowSize))),
				   	0,
				   	GL_RED,
				   	GL_FLOAT,
				   	textureData.data()
			);
			gl::Texture::GenerateMipmaps(gl::Texture::Texture2D);

		}

	AudioInput::~AudioInput() {
		reader->freeData();
	}
	
	void AudioInput::advance(double time){
		if(!reader){
			throw  RenderException("No audio data loaded");
		}
		
		if(elapsedTime >= 0.0){
			samples =  reader->getSamples(time,windowSize);
			//worker = std::async(std::launch::async, [&](){
			this->_calculateFFTData();
			//});	
			//TDO: Check if there is any performance gain from using the future.
//			if(!worker.valid()){
//				throw RenderException("AudioInput:Worker future is not valid, this should not happen!");
//			}
			tex.bind();
			tex.TexImage2D(
					gl::Texture::Texture2D, 
					0, 
					GL_R32F, 
					static_cast<GLsizei>(windowToBinSize(windowSize)), 
					static_cast<GLsizei>(textureData.size() / (windowToBinSize(windowSize))),
				   	0,
				   	GL_RED,
				   	GL_FLOAT,
				   	textureData.data()
			);
			gl::Texture::GenerateMipmaps(gl::Texture::Texture2D);
		}
		elapsedTime += time;
	}

	void AudioInput::prepareForRender(gl::Uniform& u){
		tex.bind();
		auto id = gl::Texture::ActiveTextureAtNextFreeUnit();
		u.setValue(static_cast<GLint>(id));
	}

} // namespace render
