/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config/inputfactory.h"
#include "config/audioinput.h"
#include "config/imageinput.h"
#include "config/imagesequenceinput.h"
#include "config/framebufferinput.h"

namespace config 
{
	std::shared_ptr<Input> InputFactory::generateFromNode(InputType type, tinyxml2::XMLElement* node, Config* parent){
		std::shared_ptr<Input> ptr;

		switch (type){
				case InputType::Audio:{
					auto val = std::shared_ptr<Input>(new AudioInput(parent));
					val->initFromXml(node);
				   	return val;
 	       	    }
				case InputType::Image: {
					auto val = std::shared_ptr<Input>(new ImageInput(parent));
				   	val->initFromXml(node);
					return val;	
			    } 
				case InputType::ImageSequence:{
					auto val = std::shared_ptr<Input>(new ImageSequenceInput(parent));
					val->initFromXml(node);
					return val;
				}
				case InputType::FrameBuffer:{
					auto val = std::shared_ptr<Input>(new FrameBufferInput(parent));
					val->initFromXml(node);
					return val;
				}
				case InputType::Unitialized:
					//should be unreachable, if you end up here, good luck.
					throw ConfigException("Forbidden Type 'Unitialized'");
					break;
				
		}
		return std::shared_ptr<Input>(nullptr); 
	}		
} // namespace 
