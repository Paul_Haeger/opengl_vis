/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config/audioinput.h"
#include "config/xmlconfig.h"
#include "tinyxml2.h"


namespace config 
{
		void AudioInput::initFromXml(const tinyxml2::XMLElement* node){
			auto loopAttribute = node->FindAttribute("looped");
			if(loopAttribute){
					std::string value(loopAttribute->Value());
					if (value == "true") {
							bIsLooped = true;
					}else if(value == "false"){
							bIsLooped = false;
					}else{
							
					throw config::ConfigException(std::string("Value '") + value + "' is not a valid value for Attribute 'looped'. Use either 'true' or 'false' at:" + std::to_string(node->GetLineNum()));
					}		
			}

			auto currentNode = node->FirstChildElement("path");
			if(!currentNode){
				throw ConfigException(std::string("Element 'path' not found in the element 'input' at:"));
			}

			path = currentNode->GetText();

			currentNode = node->FirstChildElement("duration");
			if(currentNode){
				try{
					duration = config::nodetof(currentNode);	
				}catch(ConfigException& e){
					throw ConfigException(std::string("Duration:") + e.what());			
				}
				if (duration < 0) {
					throw ConfigException(std::string("Duration:Negative values not allowed at:") + std::to_string(currentNode->GetLineNum()));
				}	
			}
			currentNode = node->FirstChildElement("offset");
			if(currentNode){
				try{
					offset = config::nodetof(currentNode);
				}catch(ConfigException& e){
						throw ConfigException(std::string("skip:") + e.what());
				}
			}
			currentNode = node->FirstChildElement("skip");
			if(currentNode){
				try{
					skip = config::nodetof(currentNode);
				}catch(ConfigException& e){
					throw ConfigException(std::string("Skip:") + e.what());
				}
				if(skip < 0){
					throw ConfigException(std::string("Skip:Negative values are not allowed at:" + std::to_string(currentNode->GetLineNum())));
				}
			}
			currentNode = node->FirstChildElement("name");
			if(!currentNode){
					throw ConfigException(std::string("Element 'name' not found in element 'input"));
			}
			internalName = currentNode->GetText();
			if(internalName.size() == 0 ){
					throw ConfigException(std::string("Name:Empty names are not allowed at:" + std::to_string(currentNode->GetLineNum())));
			}

			currentNode = node->FirstChildElement("windowSize");
			if(currentNode){
					try{
						windowSize = nodetoui(currentNode);
					}catch( ConfigException& e){
						throw ConfigException(std::string("WindowSize:") + e.what());
					}
					//check if number is power of 2
				if(windowSize < 1024){
					throw ConfigException("WindowSize:The size of the window must be 1024 at least.");
				}
	
				if((windowSize & (windowSize - 1)) != 0){
				//number not a power of 2
					throw ConfigException("WindowSize:"+ std::to_string(windowSize) + "is not a power of 2.");
				}	
		}

}

	unsigned int AudioInput::getSamplingRate() const {
		return parent->getSamplingRate();
	}

	unsigned int AudioInput::getFttTrail() const{
		return parent->getFftTrail();
	}

	unsigned int AudioInput::getFrameRate() const {
		return parent->getFrameRate();
	}
} // namespace config
