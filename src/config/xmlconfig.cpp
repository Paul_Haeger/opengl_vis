/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config/xmlconfig.h"
#include <cmath>
#include <regex>
#include "config/inputfactory.h"

namespace config
{

	void XmlConfig::readSettings(tinyxml2::XMLElement* node){
		auto currentNode = node->FirstChildElement("samplingRate");
		if (!currentNode) {
			throw config::ConfigException(std::string("SamplingRate: Elment 'samplingRate' not found in element 'settings'"));		
		}	
		try{
			samplingRate = config::nodetoui(currentNode);		
		}catch(config::ConfigException& e){
			throw config::ConfigException(std::string("SampleRate:") + e.what());
		}
		if(samplingRate == 0){
			throw config::ConfigException("SampleRate: Sampleling rate of 0 not allowed at: " + std::to_string(currentNode->GetLineNum()));
		}

		currentNode = node->FirstChildElement("outputDir");
		if(!currentNode){
			outputDir = ".";	
		}else{
			outputDir = currentNode->GetText();
		}

		currentNode = node->FirstChildElement("outputName");
		if(!currentNode){
			throw config::ConfigException(std::string("OutputName:Element 'outputName' not found in element 'settings'"));
		}

		outputName = currentNode->GetText();

		currentNode = node->FirstChildElement("frameRate");
		if(!currentNode){
			frameRate = 30;
		}else{
			try{
				frameRate = config::nodetoui(currentNode);
			}catch(config::ConfigException& e){
				throw config::ConfigException(std::string("Framrate:")+ e.what());
			}
			if (frameRate == 0) {
				throw config::ConfigException("Framerate: Framerate of 0 not allowed at: " + std::to_string(currentNode->GetLineNum()));
			}

			currentNode = node->FirstChildElement("fftTrail");
			if (!currentNode) {
				throw config::ConfigException(std::string("FFTTrail:Element 'fftTrail' not found in the element 'settings'"));
			}
			try{
				fftTrail = config::nodetoui(currentNode);
			}catch(config::ConfigException& e){
				throw config::ConfigException(std::string("FFTTrail:")+ e.what());
			}
			currentNode = node->FirstChildElement("duration");
			if(!currentNode){
				throw config::ConfigException(std::string("Duration: Element 'duration' not found in  element 'settings'"));
			}
			auto attribute = currentNode->FindAttribute("type");
			if(!attribute){
				throw config::ConfigException("Duration: Element 'duration' is missing the attribute 'type' at : " + std::to_string(currentNode->GetLineNum()));
			}

			std::string val(attribute->Value());
			try{
				if(val == "seconds"){
					double d = config::nodetod(currentNode);
					frameDuration = static_cast<unsigned int>(ceil(d * frameRate));
				}else if(val == "frames"){
					frameDuration = config::nodetoui(currentNode);
				}else{
					throw config::ConfigException(std::string("Unknown attribute value '") + attribute->Value()+"', only 'seconds' or 'frames' are valid at: " + std::to_string(currentNode->GetLineNum()));
				}
			}catch(config::ConfigException& e){
				throw config::ConfigException(std::string("Duration:")+e.what());
			}

			if(frameDuration == 0){
				throw config::ConfigException("Duration: Duration of 0 not allowed at : " + std::to_string(currentNode->GetLineNum()));
			}
			/* redundant check for unsigned
			   else if(frameDuration < 0){
			   throw config::ConfigException("Duration: Negative values not allowed at: " + std::to_string(currentNode->GetLineNum()));
			   }*/

			currentNode = node->FirstChildElement("resolution");
			if(!currentNode){
				throw config::ConfigException(std::string("Resolution:Element 'resolution' not found in element 'settings'"));	
			}
			try{
				resolution = config::parseResolution(currentNode);
			}catch(config::ConfigException& e){
				throw config::ConfigException(std::string("Resolution:") + e.what());
			}
			//Finished initialization of settings

		}
	}

	void XmlConfig::readInput(tinyxml2::XMLElement* node){
		auto typeAttr = node->FindAttribute("type");
		if(!typeAttr){
			throw config::ConfigException(std::string("Element missing attribute 'type' at:") + std::to_string(node->GetLineNum()));
		}
		std::string typeText(typeAttr->Value());
		config::InputType type;
		//Read the type
		if(typeText == "image"){
			type = config::InputType::Image;
		}else if(typeText == "audio"){
			type = config::InputType::Audio;	
		}else if(typeText == "imagesequence"){
			type = config::InputType::ImageSequence;
		}else{
			throw config::ConfigException("Unkown Attibute '"+ typeText + "' at: " + std::to_string(node->GetLineNum()));
		}

		//Read the Id
		auto idAttr = node->FindAttribute("id");
		if (!idAttr) {
			throw config::ConfigException(std::string("Element missing attribute 'id' at:") + std::to_string(node->GetLineNum()));
		}	
		std::string id = idAttr->Value();
		if(id.length() == 0){
			throw config::ConfigException(std::string("Empty Id not allowed at:" + std::to_string(node->GetLineNum())));
		}
		if(inputs.find(id) != inputs.end()){
			throw config::ConfigException("Id '" + id + "' not unique at:" + std::to_string(node->GetLineNum()));
		}

		std::shared_ptr<config::Input> ptr = config::InputFactory::generateFromNode(type, node, this);
		inputs.emplace(id, ptr);


	}

	void XmlConfig::loadConfig(const std::string& path){
		doc.LoadFile(path.c_str());
		//load the configuration
		tinyxml2::XMLElement* root = doc.FirstChildElement("visualizer");
		if(!root){
			throw config::ConfigException("Failed to find the root element named 'visualizer.'");	
		}
		auto settings = root->FirstChildElement("settings");
		if (!settings) {
			throw config::ConfigException("Element 'settings' not found in the element 'visualizer'");	
		}
		//Read settings
		try{
			readSettings(settings);
		}catch(config::ConfigException&	e){
			throw config::ConfigException(std::string("Settings:")+e.what());
		}
		//Traverse all inputs

		auto firstInput = root->FirstChildElement("input");
		if(firstInput){
			try{
				readInput(firstInput);
			}catch(config::ConfigException& e){
				throw config::ConfigException(std::string("Input:") + e.what());
			}
		}else{
			//TODO: Add a warning	
		}
		auto inputNode = firstInput;
		while(inputNode->NextSiblingElement("input")){
			inputNode = inputNode->NextSiblingElement("input");
			try{
				readInput(inputNode);
			}catch(config::ConfigException& e){
				throw config::ConfigException(std::string("Input:")+ e.what());
			}
		}

		//TODO: Read Programs


	}



}

