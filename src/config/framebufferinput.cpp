/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config/framebufferinput.h"
#include "config/config.h"
#include <string>

namespace config
{
		void FrameBufferInput::initFromXml(const tinyxml2::XMLElement* node) {
			auto currentNode = node->FirstChildElement("name");
			if(!currentNode){
				throw ConfigException("Elemnt 'name' not found in element 'input'");
			}
			internalName = currentNode->GetText();
			if (internalName.size() == 0) {
					throw ConfigException("Name:Empty string not allowed at:" + std::to_string(currentNode->GetLineNum()));
			}
			currentNode = node->FirstChildElement("resolution");
			if(currentNode){
				try{
				resolution = config::parseResolution(currentNode, parent->getResolution());
				}catch(ConfigException& e){
					throw ConfigException(std::string("Resolution:")+ e.what());
				}
			}
			
				
		}
} // namespace config
