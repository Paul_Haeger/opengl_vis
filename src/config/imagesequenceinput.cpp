/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config/imagesequenceinput.h"

namespace config
{
	void ImageSequenceInput::initFromXml(const tinyxml2::XMLElement* node){
		try{
			ImageInput::initFromXml(node);
		}catch(ConfigException& e){
			throw;
		}
	
		auto loopAttribute = node->FindAttribute("looped");
		if(loopAttribute){
			std::string val(loopAttribute->Value());
			if(val == "true"){
				bIsLooped = true;
			}else if(val == "false"){
				bIsLooped = false;
			}else{
				throw ConfigException(std::string("Unknown value '") + val + "' for attribute 'looped', please use 'true' or 'false' at:" + std::to_string(node->GetLineNum()));
			}
		}
		
		auto currentNode = node->FirstChildElement("start");
		if(!currentNode){
			throw ConfigException(std::string("Element 'start' not found in element 'input'"));
		}

		try{
			start = config::nodetoui(currentNode);
		}catch(ConfigException& e){
			throw ConfigException(std::string("Start:")+ e.what());
		}
		
		currentNode = node->FirstChildElement("end");
		if(!currentNode){
			throw ConfigException(std::string("Element 'end' not found in element 'input'"));
		}

		try{
			end = config::nodetoui(currentNode);
		}catch(ConfigException& e){
			throw ConfigException(std::string("End:") + e.what());
		}

		currentNode = node->FirstChildElement("frameRate");
		if (!currentNode) {
				throw ConfigException(std::string("Element 'frameRate' not found in element 'input'"));
		}
		
		try{
			frameRate = config::nodetoui(currentNode);
		}catch(ConfigException& e){
			throw ConfigException(std::string("Framerate:") + e.what());
		}

		currentNode = node->FirstChildElement("name");
		if(!currentNode){
			throw ConfigException("Element 'name' not found in element 'input'");
		}

		internalName = currentNode->GetText();

		if(internalName.size() == 0){
			throw ConfigException("Name:Empty string not allowed at:" + std::to_string(currentNode->GetLineNum()));
		}

		currentNode = node->FirstChildElement("formatString");
		if(!currentNode){
			throw ConfigException("Element 'formatString' not found in elment 'input'");
		}
		formatString = currentNode->GetText();

		if(formatString.size() == 0){
			throw ConfigException("Empty format strings not allowed");
		}
	}	
} // namespace config
