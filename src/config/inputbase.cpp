/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config/inputbase.h"
#include <string>
#include <limits>
#include <regex>
#include <cmath>
namespace config
{
		int nodetoi(const tinyxml2::XMLElement* node){
			std::string val(node->GetText());
			try{	
				return std::stoi(val);
			}catch(std::invalid_argument& e){
					throw ConfigException("Failed to parse integer '"+val+"' at:" + std::to_string(node->GetLineNum()));
			}
			catch(std::out_of_range& e){
					throw ConfigException("Integer '"+val+"' too large to represent at:" + std::to_string(node->GetLineNum()));
			}	
		}

		unsigned int nodetoui(const tinyxml2::XMLElement* node){
			std::string val(node->GetText());
			try{
				long tmp =  std::stol(val);		
				if(tmp > std::numeric_limits<unsigned int>::max()){
					throw ConfigException("Unsigned integer '"+val+"' too large to represent at:" + std::to_string(node->GetLineNum()));
				}else if(tmp < 0){
					throw ConfigException("Negative values for an unisgned int are not allowed at:" + std::to_string(node->GetLineNum()));
				} 
				//all checks passed
				return static_cast<unsigned int>(tmp);

			}catch(std::invalid_argument& e){
				throw ConfigException("Failed to parse unsigned '"+ val +"' integer at:" + std::to_string(node->GetLineNum()));
			}	
			catch(std::out_of_range& e){
				throw ConfigException("Unsigned integer '"+val+"' too large to represent at:" + std::to_string(node->GetLineNum()));
			}		
		}

		double nodetod(const tinyxml2::XMLElement* node){
			std::string val(node->GetText());
			try{
				return stod(val);
			}catch(std::invalid_argument& e){
				throw ConfigException("Failed to parse double '" + val +"' at:"+ std::to_string(node->GetLineNum()));
			}catch(std::out_of_range& e ){
					throw ConfigException("Double '"+val +"'is too small or too large to represent at:" + std::to_string(node->GetLineNum()));
			}
		}	

		float nodetof(const tinyxml2::XMLElement* node){
			std::string val(node->GetText());
			try{
				return stof(val);
			}catch(std::invalid_argument& e){
					throw ConfigException("Failed to parse float '"+val+"' at:" + std::to_string(node->GetLineNum()));
			}
			catch(std::out_of_range& e){
					throw ConfigException("Float '" + val + "'is too small or too large to represent at:" + std::to_string(node->GetLineNum()));
			}
		}

Resolution_t parseResolution(const tinyxml2::XMLElement* node,const Resolution_t& parentResolution){
		
		auto widthNode = node->FirstChildElement("width");
		auto heightNode = node->FirstChildElement("height");
		
	if (!heightNode) {
		throw config::ConfigException("Tag 'height' not found in tag 'resolution': " + std::to_string(node->GetLineNum()));
	}
	if(!widthNode){
		throw config::ConfigException("Tag 'width' not found in tag 'resolution': " + std::to_string(node->GetLineNum()));
	}	
	unsigned int width, height;	

	//Make a lambda because we need to do the same process twice.
	auto parseValFunc = [](const tinyxml2::XMLElement* node, const unsigned int parentVal) {
		//find out wether its relative or absolute
		std::string text(node->GetText());
		if(text.back() == '%'){
			//relative mode
				std::string toParse = text.substr(0, text.length() - 1);
				if(parentVal == 0){
					throw ConfigException("No specified parent width while using relative resolution width."); //A bit cryptic, could be worded better
				}
				try{
					double val = std::stod(toParse);
					if(val < 0){
						throw ConfigException("Negative values are not allowed at:" + std::to_string(node->GetLineNum()));
					}else if(val == 0){
						throw ConfigException("Value of 0 is not allowed at:" + std::to_string(node->GetLineNum()));
					}
					return  static_cast<unsigned int>(ceil((val / 100.0) * parentVal));		
				}catch(std::invalid_argument& e){
						throw ConfigException("Failed to parse double '" + toParse + "' at:" + std::to_string(node->GetLineNum()));
				}catch(std::out_of_range& e){
						throw ConfigException("Double '" + toParse + "' is too large to represent at:" + std::to_string(node->GetLineNum()));
				}
		}else if(std::regex_match(text, std::regex("\\d+(px)?"))){
			//value in pixels, either writte as 1123 or 1234px
			//remove the px part
			std::string toParse = std::regex_replace(text, std::regex("px"), "");
			try{
				int val = std::stoi(toParse);
				if(val < 0){
					throw ConfigException("Negative values not allowed at:" +std::to_string(node->GetLineNum()));
				}else if(val == 0){
					throw ConfigException("Value of 0 not allowed at:" + std::to_string(node->GetLineNum()) );
				}
				
				return static_cast<unsigned int>(val);
			}catch(std::invalid_argument& e){
				throw ConfigException("Failed to parse integer '" + toParse + "' at:" +std::to_string(node->GetLineNum()));
			}catch(std::out_of_range& e){
				throw ConfigException("Integer '" + toParse + "' is too large to represent at:" + std::to_string(node->GetLineNum()));
			}
		}else{
			throw ConfigException("Not able to find out wether value is relative or absolute at:" + std::to_string(node->GetLineNum()));
		}

	};
	

	try{
		width = parseValFunc(widthNode, parentResolution.first);
		height = parseValFunc(heightNode, parentResolution.second);
	}catch(config::ConfigException& e){
			throw;
	}

	return Resolution_t(width, height);
}


std::string toLower(const char* str, const size_t length ){
	std::string ret;
	if(length == 0){
		//Null terminated string	
		for(const char* ptr = str;*ptr != '\0'; ++ptr){
			ret.push_back(static_cast<char>(std::tolower(static_cast<unsigned char>(*ptr))));
		}
	}else{
		//Non null terminated string
		for(size_t i = 0; i < length; ++i){
			ret.push_back(static_cast<char>(std::tolower(static_cast<unsigned char>(str[i]))));
		}
	}
	return ret;
}

std::string toLower(const std::string& str){
	std::string ret = str;
	std::transform(str.begin(), str.end(), ret.begin(), [](unsigned char c){return std::tolower(c);});
	return ret;
}

} // namespace config
