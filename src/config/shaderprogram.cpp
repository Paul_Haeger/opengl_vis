/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config/shaderprogram.h"

namespace config
{

	//ShaderProgram::ShaderProgram(Config* inParent) 
	//		: parent(inParent) {}

	void ShaderProgram::initFromXml(tinyxml2::XMLElement* node){
		auto currentNode = node->FirstChildElement("fragmentShader");			
		if(!currentNode){
				throw  ConfigException("Element 'fragmentShader' not found in element 'program'");
		}
		fragmentPath = currentNode->GetText();
		
		for(auto uniformNode = currentNode; uniformNode != nullptr ; uniformNode = uniformNode->NextSiblingElement("uniform;")){
			//Check and read out all uniform nodes
			auto attrib = uniformNode->FindAttribute("type");
			if(!attrib){
				throw ConfigException("Uniform:Missing Attribute 'type' at:" + std::to_string(uniformNode->GetLineNum()));
			}
			std::string attribVal(config::toLower(attrib->Value())); //Not mentioned in the docs but it should avoid some user errors.
			config::DefaultType t = config::DefaultType::None;
			bool bIsCustom = false;
			std::string id;
			if(attribVal == "custom"){
				bIsCustom = true;
				currentNode = uniformNode->FirstChildElement("input");
				if(!currentNode){
					throw ConfigException("Uniform:Element 'input' was not found in element 'uniform' (When the type is 'custom' make sure to add the element 'input' with the desired id)");
				}
			}else if(attribVal == "time"){
				t = DefaultType::Time;
			}else if(attribVal == "frame"){
				t = DefaultType::Frame;
			}else if(attribVal == "framerate"){
				t = DefaultType::Framerate;
			}else if(attribVal == "resolution"){
				t = DefaultType::Resolution;
			}else if(attribVal == "samplingrate"){
				t = DefaultType::SamplingRate;
			}else if(attribVal == "duration"){
				t = DefaultType::SamplingRate;
			}else if(attribVal == "ffttrail"){
				t = DefaultType::FFFTrail;
			}else {
				throw ConfigException("Uniform:Uknown attribute at:" + std::to_string(uniformNode->GetLineNum()));
			}
					
			currentNode = node->FirstChildElement("name");
			if (!currentNode) {
					throw ConfigException("Uniform:Element 'name' not found in element 'uniform'");
			}
			std::string name = currentNode->GetText();
			uniforms.push_back(UniformData(bIsCustom, name, t, id));	
		}
		currentNode = node->FirstChildElement("output");
		if(currentNode){
			outputId = currentNode->GetText();
		}
	}


} // namespace config
