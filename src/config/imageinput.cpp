/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "config/imageinput.h"

namespace config
{
	void ImageInput::initFromXml(const tinyxml2::XMLElement* node){
		auto currentNode = node->FirstChildElement("path");
		if(!currentNode){
			throw ConfigException(std::string("Element 'path' not found in element 'input'"));
		}
		path = std::string(currentNode->GetText());
		if(path.size() == 0){
				throw ConfigException(std::string("Path:Empty path not allowed at:" + std::to_string(currentNode->GetLineNum())));
		}	

		currentNode = node->FirstChildElement("wrap");
		if (currentNode) {
			//Not documented in the docs currently but this will hopefully avoid some user errors.
			const char* str = currentNode->GetText();
			std::string val = config::toLower(str);
			//Convert to lower	
			if(val == "Repeat"){
				wrap = Wrap::Repeat;
			}else if(val == "mirrored repeat"){
				wrap = Wrap::MirroredRepeat;
			}else if(val == "clamp to edge"){
				wrap = Wrap::ClampToEdge;		
			}else if(val == "clamp to border"){
				wrap = Wrap::ClampToBorder;
			}else{
				throw ConfigException(std::string("Wrap:Unkown option '") + std::string(str) + "' at:" + std::to_string(currentNode->GetLineNum()));
			}	
		}

		currentNode = node->FirstChildElement("filtering");
		if(currentNode){
			//same deal as above
			const char* str = currentNode->GetText();
			std::string val = config::toLower(str);
			if(val == "linear"){
				filter = Filtering::Linear;
			}else if(val == "nearest"){
				filter = Filtering::Nearest;	
			}else{
				throw ConfigException("Filtering: Unknown option '" + std::string(str) + "' at:" + std::to_string(currentNode->GetLineNum()));
			}
		}

		currentNode = node->FirstChildElement("name");
		if(!currentNode){
			throw ConfigException("Element 'name' not found in elment 'input'");
		}

		internalName = currentNode->GetText();
		if(internalName.size() == 0){
			throw ConfigException("Name:Empty string not allowed at:" + std::to_string(currentNode->GetLineNum()));
		}

	}
} // namespace config
