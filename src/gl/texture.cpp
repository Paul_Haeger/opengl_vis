/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gl/texture.h"

namespace gl
{

std::bitset<GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS - 1> Texture::isActivated = std::bitset<GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS -1>();

Texture::Texture(Type textureType ) 
	: type(textureType) {
			glGenTextures(1, &texture);  
	}

Texture::~Texture(){
		glDeleteTextures(1,&texture);
}

void Texture::bind() const {
	glBindTexture(type, texture);
	glTexParameteri(type, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(type, GL_TEXTURE_WRAP_T, wrap);	
	glTexParameteri(type, GL_TEXTURE_MIN_FILTER, filtering);
	glTexParameteri(type, GL_TEXTURE_MAG_FILTER, filtering);
}

void Texture::ActiveTexture(GLenum unit){
	glActiveTexture(GL_TEXTURE0 + unit);
	isActivated[unit] = true;
}

void Texture::TexImage2D(Type target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* data){
	glTexImage2D(target, level, internalFormat, width, height, border, format, type, data);
	
}

void Texture::ClearActivationSet() {
	isActivated.reset();
}

GLenum Texture::ActiveTextureAtNextFreeUnit(){		
	for(GLenum i = 0; i < isActivated.size(); ++i){
		if(!isActivated[i]){
			Texture::ActiveTexture(i);
			return i;
		}
	}
	throw std::out_of_range("All texture units in use");

}

void Texture::GenerateMipmaps(Type target){
	 glGenerateMipmap(target);
}

} // namespace gl
