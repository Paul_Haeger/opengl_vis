/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gl/vertexarrayobject.h"

namespace gl 
{
		
VertexArrayObject::VertexArrayObject(){
	glGenVertexArrays(1, &vao);	
}

VertexArrayObject::~VertexArrayObject(){
		glDeleteVertexArrays(1, &vao);
}

void VertexArrayObject::bind() const {
	 glBindVertexArray(vao);
}

void VertexArrayObject::vertexAttribPointer(GLuint index, GLint size, GLenum type,	GLboolean normalized,  	GLsizei stride, const void * pointer){
	glVertexAttribPointer(index, size, type, normalized, stride, pointer);
}

void VertexArrayObject::enableVertexAttribArray(GLuint index){
	glEnableVertexAttribArray(index);
}

void VertexArrayObject::disableVertexAttribArray(GLuint index){
	glDisableVertexAttribArray(index);
}
}
