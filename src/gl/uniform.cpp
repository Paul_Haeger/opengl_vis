/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gl/uniform.h"
#include "gl/shaderprogram.h"

namespace gl
{
	

Uniform::Uniform(const ShaderProgram& p,const std::string& name){
	location = p.getUniformLocation(name);
	if(location == -1){
		throw UniformException("Uniform does not exists, check if the uniform is used in the shader code.");
	}
}

Uniform::Uniform(const Uniform& rhs){
		location = rhs.location;
}

Uniform& Uniform::operator=(const Uniform& rhs){
	location = rhs.location;
	return *this;
}

} // namespace gl
