/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gl/shaderprogram.h"
#include "gl/shaderexception.h"
namespace gl {
		

ShaderProgram::ShaderProgram(){
		program = glCreateProgram();
}

ShaderProgram::~ShaderProgram() {
	glDeleteProgram(program);
}

void ShaderProgram::attachShader(const Shader& s){
	glAttachShader(program ,s.getShaderObject());

}

void ShaderProgram::linkProgram(){
		glLinkProgram(program);
		GLint ret = 0;
		glGetProgramiv(program, GL_LINK_STATUS, &ret);
		if(ret == GL_FALSE) {
			//Error, get the error message
			GLchar buff[512];
		    glGetProgramInfoLog(program, 512, NULL, buff);
			throw ShaderException("Failed to link shaderprogram: " + std::string(buff));	
		}
}

void ShaderProgram::useProgram() const {
		glUseProgram(program);
}

GLint ShaderProgram::getUniformLocation(const std::string& val) const{
		return glGetUniformLocation(program, val.c_str());
}
}
