/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gl/shader.h"
#include <fstream>
#include <vector>
#include "gl/shaderexception.h"

namespace gl{
		

Shader::Shader() {}

Shader::Shader(const std::string& code, Type type){
		fromCode(code, type);
}

Shader::Shader(const char* code, Type type){
		fromCode(code, type);
}

Shader::~Shader(){
	free();
}


void Shader::fromCode(const std::string& code, Type type){
	shader = glCreateShader(type);
	if(shader != 0){
	shaderCode = code;
	bIsCreated = true;
	}else{
		throw ShaderException("Failed to create the Shader");		
	}
}

void Shader::fromFile(const std::string& path, Type type){
		std::ifstream ifs(path, std::ifstream::in);
		if(ifs.is_open()){
						shaderCode =  std::string( (std::istreambuf_iterator<char>(ifs) ),
                       (std::istreambuf_iterator<char>()));
						shader = glCreateShader(type);
						if(shader == 0){
							throw ShaderException("Failed to create the Shader");
						}
						bIsCreated = true;
	}else{
			throw ShaderException("Failed to open file " + path);
	}
}

void Shader::compile(){
	if(bIsCreated){
		const char* code = shaderCode.c_str();
		glShaderSource(shader, 1, &code, NULL);
		glCompileShader(shader);
		GLint ret = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &ret);
		if(ret == GL_FALSE){
			//Get the error log
			GLint len = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
			if (len > 0) {

				//GLchar buff[512] =  {'\0'}; //Create a null terminated buffer
				std::vector<GLchar> buff(static_cast<size_t>(len), '\0');

				glGetShaderInfoLog(shader, len, NULL, buff.data());
				std::string msg(buff.data());
				throw ShaderException("Failed to compile shader: " + msg);
			}
		
				throw ShaderException("Failed to compile shader: No log message returned.");
		}
		bIsCompiled = true;
	}else{
		throw ShaderException("Shader not created");
	}

}

void Shader::free() noexcept{
	if(bIsCreated){
		glDeleteShader(shader);
		shaderCode = "";
		bIsCreated = false;
		bIsCompiled = false;
	}
}
}
