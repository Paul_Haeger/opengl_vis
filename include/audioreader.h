/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string>
#include <cstdint>
#include <vector>
#include "audioreaderexception.h"
#include "sndfile.hh"



/** \brief Class that loads and stores Audio samples.
 *
 * When this class is initalized the Audio will be loaded from the specified path. The samples will be
 * encoded in 32bit floating point format. If there are multiple channels they will be mixed down to mono.
 * The samples will be resampled to the desired sampling rate. Any file that ffmpeg can read can be accepted.
 * All sampling data will be loaded into memory at creation. There is sadly no streaming as of now. 
 **/
class AudioReader {


	int sampleRate;
	//loat* data = nullptr; //Sample buffer
	std::vector<float> data;
	std::size_t dataSize = 0; //Size of buffer
	std::size_t dataIndex = 0; //Index for the current read location


public:

	/** \brief Initializes and reads out a audio file and buffers it.
	 * Decodes and resamples the audio file using FFmpeg and loads the data into a buffer.
	 * \note With long audio files there might be a large memory footprint as all the data gets written into one buffer.
	**/
	AudioReader(std::string filename, int desiredSampleRate);	
	
	~AudioReader();

    /**
	 * Frees the buffer from data. The class cant be used anymore afterwards. Called at deconstruction.
	**/
	void freeData();

	/**
	* Returns the current readerhead position in seconds.
	 **/
	double getPlaybackTime() const;

	/**
	 * Sets the readerhead to the specified time in seconds.
	**/
	void setPlaybackTime(double time);

	/** Gets the current index position of the readerhead.
	 	 **/
	std::size_t getPlaybackIndex() const;

	/** Sets the readerhead to the specified index.
	 * \param index The new index position.
	**/
	void setPlaybackIndex(std::size_t index);

	/** \brief Gets windowSize samples from the specified duration.
	 * The latest values will always be taken. If the buffer is to small from the left, 0s will be prepended. If the buffer is too small from the right, 0s will be appended.
	 * The seekerhead will be advanced by duration. The duration will be clipped to the length, so you cant read outside the buffer.
	 * \param duration The duration of the timespan where we want our samples from. Behaviour for negative values are undefined. 
	 * \param windowSize Specifies how many samples should be returned.
	 * \return Returns a std::vector<float> with all samples
	 **/
	std::vector<float> getSamples(double duration, unsigned int windowSize);
	
};
