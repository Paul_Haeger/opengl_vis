/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_INPUT_FACTORY_H__
#define __RENDER_INPUT_FACTORY_H__

#include <memory>
#include "render/setupexception.h"

namespace config
{
	class Input;
} // namespace config


namespace render
{
	class Input;
	class RenderContext;	

	/** \brief Interface that will be used to construct the appropiate input object.
	 *
	 * This class uses the abstract factory pattern.
	 **/
	class InputFactory{
		
		public:
		InputFactory() {}
		virtual ~InputFactory() {}

		virtual std::shared_ptr<Input> constructInput(RenderContext* parent, const config::Input* input) = 0;
	};
} // namespace render


#endif 
