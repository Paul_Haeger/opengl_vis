/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_SHADER_PROGRAM_H__
#define __RENDER_SHADER_PROGRAM_H__
//This glues config and gl together
#include "gl/shaderprogram.h"
#include "gl/uniform.h"
#include <vector>
#include <memory>
#include <render/setupexception.h>

namespace config		
{
	class ShaderProgram;
} // namespace config 

namespace render
{
	class RenderContext;

	class ShaderProgram{
		gl::ShaderProgram program;
		std::vector<std::shared_ptr<gl::Uniform>> Uniforms;
		RenderContext* parent;


	public:
		
		
		/** \brief Static method to generate a vertex shader.
		 * Generates the corresponding vertex shader to the given fragment shader.
		 * \param[in] path  Path of the fragment shader file.
		 * \return Returns the shader code.
		 * \throw SetupException
		 */
		static std::string generateVertexShader(const std::string& path);

		/**
		 * Compiles the shaders.
		 * \param[in] data The config object with all the needed data
		 *
		 * \throw SetupException
		 */
		void applyShadersProgram(const config::ShaderProgram& data);

		inline gl::ShaderProgram& getProgram() {return program;}
		
		void use() const;
	};

} // namespace render



#endif 
