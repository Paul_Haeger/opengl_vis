/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_IMAGE_SEQUENCE_INPUT_H__
#define __RENDER_IMAGE_SEQUENCE_INPUT_H__

#include "gl/texture.h"
#include "render/input.h"
#include "render/imageinput.h"
#include "render/renderexception.h"

namespace render
{
		/** \brief Implements the logic to make an image sequence aviable as a texture.
		 **/
		class ImageSequenceInput : public ImageInput {
	
			unsigned int start, end;
			unsigned int frameRate;
			unsigned int currentFrame;
			double elapsedTime = 0.; //Internal variable to keep track of the time
			bool bIsLooped;
			std::string path;
			std::string fileEnding;
			std::string formatString;

		public:

		ImageSequenceInput(RenderContext* inParent, unsigned int inStart, unsigned int inEnd,
				unsigned int inFrameRate, bool inBIsLooped, std::string inPath, std::string inFileEnding, std::string inFormatString,
				gl::Texture::Wrapping wrap, gl::Texture::Filtering filtering);

		/**
		 * \brief Reads out the next image for the next frame if necessary.
		 * If the current time increment is great enough to warrant a new frame load,
		 * so that the sequence can be displayed in the specified framerate, then the method will 
		 * load the next image.
		 * The texture will be made active in this call.
		 *
		 * \param[in] deltaTime The timeincrement. This is the elasped time between the previous and this render.
		 **/
		virtual void advance(double deltaTime) override;
		
		/**
		 * \brief Links the internal texture object to the uniform, so the shader can read from it.
		 * This method is called before every shader draw.
		 *
		 * \param[in] u The uniform the texture should be linked to.
		 **/
		virtual void prepareForRender(gl::Uniform& u) override; 

		/**
		 * Sets up the render input class according to the configuration.
		 * \param[in] input  Base class pointer of the config::ImageSequenceInput class
		 *
		 * \throw SetupException
		 * \pre <tt>input != NULL</tt>
		 */
		virtual void implementConfiguration(const config::Input* input) override;


				
		};
} // namespace render

#endif 
