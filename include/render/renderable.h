/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER__RENDERABLE_H__
#define __RENDER__RENDERABLE_H__

namespace gl
{
	class FrameBuffer;
	class Texture;
} // namespace gl


namespace render
{
	

	/**
	 * \brief Interface describing targets we can render to.
	 *
	 **/
	class Renderable {
		
		public:
			Renderable();
			
			/**
			 * \brief Binds the rendertarget so that all following draw calls will draw to the target.
			**/
			virtual void bindRenderTarget() = 0;

			/**
			 * \brief Called when another rendertarget is bound.
			 **/
			virtual void unbindRenderTarget() = 0;

			/**
			 * \brief Returns the buffer we made the draw calls on-
			 *
			 * This is needed so you can extract the image date from the buffer and save it on the disk if necessary.
			 * \return A reference to the framebuffer that was rendered to.
			 **/
			virtual gl::FrameBuffer& getTargetFrameBuffer() const = 0;
		  	
			/**
			 * \brief Returns the texture that contains the result of the framebuffer.
			 *
			 * This is necessary for rendering the output to the framebuffer provided by the context.
			 *
			 * \return A reference to the texture that hold the rendered output.
			 **/
			virtual gl::Texture& getTargetTexture() const = 0;	
	};
} // namespace render
#endif
