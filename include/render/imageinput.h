/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_IMAGE_INPUT_H__
#define __RENDER_IMAGE_INPUT_H__

#include "render/input.h"
#include "gl/texture.h"
#include "render/setupexception.h"


namespace render
{
		
	class ImageInput : public Input {
	
	protected:
			gl::Texture tex = gl::Texture(gl::Texture::Texture2D);
			unsigned char* data;
			int width, height, nrChannels;
			bool bIsLoaded = false;

			/**
			 * \brief Loads the texture from drive and puts in into a texture.
			 *
			 * The specified file will be loaded from the disk and stb_image will attempt to read it.
			 * If successful, then there will be a gl::Texture initialized, with the in the class specified wrapping
			 * and filtering. If the loading fails, then a SetupException is thrown.
			 * \param[in] path  The path the image should be read from.
			 *
			 * \throw SetupException
			 * \see gl.Texture
			 */
			void loadTexture(const std::string& path);

			/**
			 * \brief Protected constructor for deriving classes to use.
			 *
			 * This protected constructor is for derived classes. This constructor does not call loadTexture() and
			 * only sets the specified values.
			 * \param[in] inParent  The parenting context for this input.
			 * \param[in] wrap  The wrap behavior of the texture.
			 * \param[in] filtering  The filtering behaviour of the texture.
			 *
			 * \pre <tt>inParent != NULL</tt>
			 */
			ImageInput(
					RenderContext* inParent, 
					gl::Texture::Wrapping wrap, 
					gl::Texture::Filtering filter
			);

	public: 

			/**
			 * \brief Loads the texture from disk with the specified values.
			 * Loads the texutes from disk with the specified values using stb_image. If this fails, then a 
			 * SetupException will be thrown. On success a gl::Texture will be created with the
			 * image inside.
			 * \param[in] inParent  The parenting context.
			 * \param[in] imagePath  The path of the image file to load.
			 * \param[in] wrap  The wrap behavior of the texture.
			 * \param[in] filtering  The filtering behaviour of the texture.
			 *
			 * \throw SetupException
			 * \see gl.Texture 
			 *
			 * \pre <tt>inParent != NULL</tt>
			 */
		ImageInput(
				RenderContext* inParent,
				const std::string& imagePath, 
				gl::Texture::Wrapping wrap, 
				gl::Texture::Filtering filtering
		);

		~ImageInput() override;

		virtual void advance(double time) override;
		virtual void prepareForRender(gl::Uniform& u) override; 

	};
} // namespace render

#endif 


