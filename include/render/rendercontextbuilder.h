/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_CONTEXT_BUILDER_H__
#define __RENDER_CONTEXT_BUILDER_H__
#include <memory>
#include "render/setupexception.h"

namespace config {
	class Config;
}


namespace render
{

	class RenderContext;

	class RenderContextBuilder{

	public:


		/**
		 * \brief Constructs a render context object with all the neccesary settings.
		 *
		 *	If the setup fails a SetupException is thrown.
		 *
		 *	\see XmlConfig RenderContext
		 *
		 * @param[in] config  The config that specifies the setup.
		 *
		 * @return The constructed context.
		 * @throw SetupException
		 */
		static std::shared_ptr<RenderContext> constructContext(const config::Config& config);



	};

}
#endif 


