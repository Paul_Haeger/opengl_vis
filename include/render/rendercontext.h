/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_CONTEXT_H__
#define __RENDER_CONTEXT_H__
#include "gl/shaderprogram.h"
#include "gl/uniform.h"
#include <memory>
#include <vector>
#include <map>
#include <deque>
#include "config/inputbase.h"

class XmlConfig;



namespace render
{
	
class RenderContextBuilder;
class Input;

/**
 * Class that glues all the rendercode together.
 * 
 *
 */
class RenderContext {
	float elapsedTime = 0.0f; /** The time that aleady elapsed in this context*/
	unsigned int frameRate; /** The framerate */
	unsigned int frameCount; /** The number of maximal frames*/
	config::Resolution_t resolution; /** Parent resolution*/	

	std::vector<std::shared_ptr<gl::ShaderProgram>> programStack; /** The stack of programs to render*/

	std::map<std::string, std::shared_ptr<render::Input>> inputs; /** This associates the id with the input*/
		
	std::map<std::string ,std::deque<std::shared_ptr<gl::Uniform>>> customDataUniforms; ///This associates the uniform with the input id.
	std::map<config::DefaultType, std::deque<std::shared_ptr<gl::Uniform>>> defaultDataUniforms; ///This associates the uniforms with the standard inputs aviable	
	
	friend RenderContextBuilder;		

	public:

	RenderContext(unsigned int inFrameRate, unsigned int inFrameCount, const config::Resolution_t& res);
	/**
	 * Will implement and link all shader programs, inputs and everything else specified in the config.
	 * \param[in] config  The config to implement.
	 *
	 */
		void implementConfiguration(const XmlConfig& config);

		void addUniform(const std::string& id, std::shared_ptr<gl::Uniform> uniform);

		void addUniform(config::DefaultType type, std::shared_ptr<gl::Uniform>);
};


} // namespace render{

#endif 


