/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_AUDIO_INPUT_H__
#define __RENDER_AUDIO_INPUT_H__
#include "render/input.h"
#include "audioreader.h"
#include <memory>
#include "gl/texture.h"
#include "render/renderexception.h"
#include "tools/kiss_fftr.h"
#include <future>


namespace render
{
	
	class AudioInput : public Input {
			
		public:
			class FFTDeleter{
				public:
				void operator()(kiss_fftr_cfg* cfg) const;
			};	
		private:
			std::unique_ptr<kiss_fftr_cfg, AudioInput::FFTDeleter> cfg;
			std::unique_ptr<AudioReader> reader;	
			gl::Texture tex;
			bool bIsLooped; /*** Is the sound looped ?*/
			double elapsedTime = 0.0; /*** How long has the song been playing?*/
			double skip = 0.0;	/*** Homw many secionds of this sound be skipped?*/	
			unsigned int samplingRate;	/*** The sampling rate*/
			unsigned int windowSize; /*** The window size for the fft*/

			std::vector<float> textureData;

			std::future<void> worker; //stored here for persistence. Is needed when rendering starts
			std::vector<float> samples; //stored here for presistence.
			std::vector<kiss_fft_cpx> complexPoints;

			void _calculateFFTData();


			/**
			 * \brief Calculates the number of bins the window with the size will create.
			 *
			 * \param window The window size.
			 * \return The number of bins.
			 */
			inline unsigned int windowToBinSize(unsigned int window){
				return window/2 + 1;
			}

		public:
			
			/**
			 * \brief Initializes the renderer with the parent.
			 * \param[in] inParent  Renderapplication parent.
			 *
			 * \pre <tt>inParent != NULL</tt>
			 */
			AudioInput(
					RenderContext* inParent, 
					bool inBIsLooped, 
					double inOffset,
					double inElapsedTime,
				   	double inSkip,
				   	unsigned int inSamplingRate,
				   	unsigned int inWindowSize,
					unsigned int fftTrail,
				   	const std::string& file 
					);

			~AudioInput();

			/**
			 * \brief Advances the internal counter and loads the new data.
			 * Loads the new frames so they can be fed through the fft later.
			 * \param[in] time  How many seconds to advance.
			 *
			 * \throw RenderException
			 */
			virtual void advance(double time) override;
			virtual void prepareForRender(gl::Uniform& u) override;
			
	};
} // namespace render


#endif 
