/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_FRAME_BUFFER_INPUT__
#define __RENDER_FRAME_BUFFER_INPUT__

#include "config/inputbase.h"
#include "render/input.h"
#include "render/renderable.h"
#include "gl/framebuffer.h"
#include <array>


namespace render
{
		/** \brief The rendering implementation of a framebuffer.
		 * Two Frame buffers and textures to allow reading of the previous draw call will be initialized.  
		 **/
		class FramebufferInput : public Input, public Renderable {
			std::array<gl::FrameBuffer, 2> buffers; //2 textures and buffers to allow 'self read' 
			std::array<gl::Texture, 2> textures;		
			unsigned char currentBuffer = 0; //Contains the index for the current texture to read.
			bool bIsBeingRenderedTo = true; //Make sure we dont swap the buffers when we are only reading from it.

			void swapBuffers();

			public:
			FramebufferInput(RenderContext* inParent,  config::Resolution_t res);			

			virtual void advance(double time) override;

			virtual void prepareForRender(gl::Uniform& u) override;

			virtual void bindRenderTarget() override;

			virtual void unbindRenderTarget() override;

			virtual gl::FrameBuffer& getTargetFrameBuffer() override; 

			virtual gl::Texture& getTargetTexture() override;
			
		};
} // namespace render

#endif 

