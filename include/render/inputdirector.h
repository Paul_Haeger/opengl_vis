/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __REDER_INPUT_DIRECTOR_H__
#define __REDER_INPUT_DIRECTOR_H__

#include <memory>

namespace config
{
	class Input;
} // namespace config


namespace render
{
	class InputFactory;
	class RenderContext;

	/** \brief Class that will decide and run the appropiate factory object.
	 * 
	 **/
	class InputDirector{

	public:	
		static std::unique_ptr<render::InputFactory> getFactory(const config::Input& input);  

	};

}

#endif
