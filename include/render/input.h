/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __RENDER_INPUT_H__
#define __RENDER_INPUT_H__

#include "config/inputbase.h"
#include "render/setupexception.h"
#include "gl/uniform.h"



/** \brief Contains the implementation necessary for rendering.
 **/
namespace render
{
	class RenderContext;


/** \brief Base class for the implementation of the inputs
 *  The classes derived from this class will store the data necessary and the function calls necessary
 *  to link the data to the shader. The given format should be at the least be documented in the 
 *  configuration documentation.
 *  The names mirror that of the config namespace .
 */
	class Input {
		
		protected:

		RenderContext* parent;

		public:
			Input() = delete;
			virtual ~Input() {};

			Input(RenderContext* inParent) : parent(inParent) {}

			/**
			 * \brief Implement this if some preparation before the frame draw is needed.
			 * If you need to load or calculate something, a texture for example, then you need to implement this method. This method will be called at the beginning of each frame.
			 * If something fails you need to throw a RenderException
			 * \param[in] deltaTime  The time increment in seconds. If the application is run in real time this will be the time the last draw call took, if not it will be the time between frames using the specified frame rate.
			 * \throw RenderException
			 * \see RenderException
			 */
			virtual void advance(double deltaTime) {}

			/**
			 * \brief Implement this if you need to do some preparations before a draw call. 
			 * Each time before a shader is rendered this method will be called. It is very likely 
			 * that you have to implement this function as you usually need to bind an uniform to something. 
			 * If something fails you need to throw a RenderException
			 *
			 * \param[in] u The uniform that should be linked to your input
			 *
			 * \throw RenderException
			 */
			virtual void prepareForRender(gl::Uniform& u) {}

			/**
			 * \brief This needs to be implemented. With this you initialize the class according to the specified configuration.
			 * You should check the type of the input first. If the input type is the wrong one a SetupException should be thrown. If any configuration is invalid, a SetupException should be thrown.
			 * \throw SetupException
			 * \see SetupException
			 **/
			virtual void implementConfiguration(const config::Input* input) {}; //TODO: Remove
	};
} // namespace render


#endif 
