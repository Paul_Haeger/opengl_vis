/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __IMAGE_SEQUENCE_IMPUT_H__
#define __IMAGE_SEQUENCE_IMPUT_H__
#include "config/imageinput.h"
#include "config/inputbase.h"
#include "config/configexception.h"

namespace config
{
	class ImageSequenceInput : public ImageInput{

		bool bIsLooped = false;
		unsigned int start;
		unsigned int end;
		unsigned int frameRate;
		std::string formatString;
		public:
		ImageSequenceInput(Config* parent) : ImageInput(parent) {in = InputType::ImageSequence;}
		virtual void initFromXml(const tinyxml2::XMLElement* node) override;

		inline bool isLooped() const {return bIsLooped;}
		inline unsigned int getStart() const {return start;}
		inline unsigned int getEnd() const {return end;}
		inline unsigned int getFrameRate() const {return frameRate;}
		inline std::string getFormatString() const {return formatString;}
	};	

} // namespace config

#endif 
