/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __IMAGE_INPUT_H__
#define __IMAGE_INPUT_H__
#include "config/inputbase.h"
#include "config/configexception.h"
#include "gl/glheader.h"
#include "gl/texture.h"

namespace config
{
		class ImageInput : public Input {
			public:
				/*«»enum Filtering : GLenum{
					Linear = GL_LINEAR,
					Nearest = GL_NEAREST
				};

				enum Wrap : GLenum{
					Repeat = GL_REPEAT,
					MirroredRepeat = GL_MIRRORED_REPEAT,
					ClampToEdge = GL_CLAMP_TO_EDGE,
					ClampToBorder = GL_CLAMP_TO_BORDER
				};*/
				typedef gl::Texture::Wrapping Wrap;
				typedef gl::Texture::Filtering Filtering;
			protected:
								std::string path;
				Filtering filter = Filtering::Nearest;
				Wrap wrap = Wrap::Repeat; 
			public:
			ImageInput(Config* parent) : Input(parent) {in = InputType::Image;}
			virtual void initFromXml(const tinyxml2::XMLElement* node) override;		

			inline Filtering getFilering() const {return filter;}
			inline Wrap getWrap() const {return wrap;}
			inline std::string getPath() const {return path;}
				
		};
} // namespace config




#endif 
