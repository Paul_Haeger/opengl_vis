/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __XML_CONFIG_H__
#define __XML_CONFIG_H__ 
#include "tinyxml2.h"
#include "config/config.h"
#include <string>
#include <gl/shader.h>
#include <gl/shaderprogram.h>
#include <gl/uniform.h>
#include <config/inputbase.h>
#include <config/configexception.h>
#include <utility>
#include <memory>

/** \page Structure of the XML file
 * The structure of an XML file should be like this (| represents an exclusive or choice):
 * <visualizer>
 * 		<settings>
 * 			<samplingRate>...</samplingRate> <!--41100kHz is the usual value.-->
 * 			<outputDir>...</outputDir> <!--Optional, will be in the config folder by default-->
 * 			<outputName>...</outputName> <!--Name of the output, will have the frame number appended. Output will always be an image sequence.-->
 * 			<frameRate>...</frameRate> <!--Optional, will be 30fps by default.-->
 *			<fftTrail>...</fftTrail> <!--How many frames should be stored of the previous frequency spectrums. Can be 0.-->
 *			<duration type="seconds | frames">...</duration <!--Desired duration either in seconds or in frames. Frames will be more accurate, but seconds are more user friendly. 
 *			With seconds the output might be a bit longer than the desired time due to the quantisation that is required to represent time in frames. 
 *			So with a framerate of 1 fps and a desired time of 1.5 seconds you will end up with 2 frames. 
 *			So the total duration in this example will be 2 seconds. But for most uses this shouldnt matter too much :).-->
 *			<resolution> <!--Project resolution-->
 *				<width>...</width> <!--Height and width in pix, can be either 123px or just 123.-->
 *				<height>...<height> <!--Same as above but for heigth.-->
 *			</resolution>
 *		</settings>
 *		<input type="image" id="...">
 * 			<path>...</path>
 * 			<wrap>Repeat | Mirrored Repeat | Clamp to Edge | Clamp to Border</wrap> <!--Optional, Repeat by default-->
 * 			<filtering>Linear | Nearest</filtering>  <!--Optional, nearest by default-->
 * 			<name>...</name> <!--Internal name to be used in the shader-->
 *		</input>	
 *		<input type="imagesequence id="..." looped="true | false"> <!--looped attribute is optional, false by default-->	
 * 			<path>...</path> <!--path to one of the sequence images.-->
 * 			<wrap>Repeat | Mirrored Repeat | Clamp to Edge | Clamp to Border</wrap> 
 * 			<filtering>Linear | Nearest</filtering> 
 * 			<name>...</name> <!--Internal name to be used in the shader-->
 *			<start>...</start> <!--Start frame-->
 *			<end>...</end> <!--End frame-->
 *			<frameRate>...</frameRate>  <!--Framerate of the image sequence.-->
 * 			<formatString>...</formatString> <!--How the frame number is formatted. Can be any valid printf string that prints decimals.
 *   		Example: "%d" for just the number, "%03d" for a number with add leading zeros to get to 3 digits('001', '014', '100', ..).-->
 *		</input>
 *		<input type="audio" id="..." looped="true | false"> <!--looped attribute is optional, will be false by default-->
 *			<path>...</path>
 *			<duration>...</duration> <!--Optional, plays the audio for specified duration in seconds, will be the full song by default. 0 is interpreted as full duration.-->
 *			<offset>...</offset> <!--Optional, defines after how many seconds the audio should start playing in the project, will be 0 by default.-->
 *			<skip>...</skip> <!--Optional, defines how many seconds of the audio should be skipped before playing, 0 by default.-->
 *			<windowSize>...</windowSize> <!-- Optional,sSpeciefies the window size of the fast fourier transform. Must be 1024 at minimum and a power of 2. Default will be 1024.
 *			The fft window specifies how many samples should be processed. Higher numbers will give more frequency resolution, but will decrease temporal resolution and increase computation time.
 *			Some formulars: Let Samlingrate \f$= \phi\f$, Sample count \f$= N\$f. 
 *			The temporal resolution is \f$t_r = frac{N,\phi}\f$. 
 *			The frequency band resolution is \f$r = frac{\phi, N}\f$ and the bin count is \f$ N_b = frac{N,2}\f$.
 *			So for example a signal sampled at 44100Hz with a window size of 1024 will have a temporal resolution of 0.023s or 23ms and a frequency band resolution of 43.066 hz per band split into 512 bins.
 *			Generally a lower temporal resolution shouldnt be a problem unless you need to detect transients that are shorter than the temporal resolution. 
 *			Consult http://support.ircam.fr/docs/AudioSculpt/3.0/co/Window%20Size.html for more information.-->
 *			<name>...</name> <!--Name for the frequency spectrum texture that is used in the shader.-->
 *		</input> 
 *		<input type="framebuffer" id="...">
 *			<name>...</name> 	<!--Name of the texture this frambuffer will represent.-->
 *			<resolution> <!--Optional, frame buffer resolution, default will be the project resolution.-->
 *				<width>...</width> <!--Either written as 123px for pixels or 200% as a scaling factor of the main resolution-->
 *				<height>...</height> <!--Same as above-->
 *			</resolution>
 *		</input>
 *		<program> <!--A shader program-->
 *			<!--Custom models and vertexshaders not supported in the moment, keeping it simple first-->
 *			<fragmentShader>...</fragmentShader> <!--Path to the fragment shader-->
 *			<uniform type="custom | time (float, in seconds) | frame (int) | framerate (int, in fps) | resolution (vec2) | samplingRate (int) | duration (int ,in frames) | fftTrail (int, How many frames are stored in the texture)" >
	 *			<!--Either use a custom value from one of the inputs or use the standard ones.-->
 *				<input>...</input> <!--Required when type is custom, ignored when not. Takes the input id of the desired input.
 *			 		-Inputs of type audio will be a 2 dimensional texture of an Fourier Transform(Frequency Spectrum). u is between 0 and 1 and will represent the frequency 1 to (samplingRate -1)/2.
 *					v is between 0 and 1 and represents the frequency specrums over time. 0 is the spectrum of the current frame and 1 is last saved frequency spectrum defined in the config.
 *					-Inputs of the type image or image sequence will be an a 2 dimensional texture of the specified image. For image sequences the current frame will be in the texture.
 *					-Inputs of the type framebuffer will be a 2 dimensional texture. They can be used as an output of other programs.-->
 *				<name>...</name> <!--Name of the uniform used in the shader. This allows for great flexibility and copying shaders you wrote from bonzomatic, shadertoys, etc -->
 *			</uniform> 
 *			<output>...</optional><!--Optional. Defines the id of the framebuffer the output should be written to. If not present, then it will be written into the main buffer.-->
 *		</program>
 *		
 *	    <!--Additional shader programs here. Use as many as you wish and your computer can take :)-->
 *		
 *</visualizer>	
 *----------------------------		
 *
 *
 *
 *		
 *			
 * 	**/

 namespace config
{

	/** \brief Reads in the configuration from the xml file.
	 *  If there are errors while processing a config::ConfigException will be thrown.
	 *  \see Structure of the XML file
	 * 
	 **/
	class XmlConfig : public Config{

		private:
			tinyxml2::XMLDocument doc;

			void readSettings(tinyxml2::XMLElement* node);
			void readInput(tinyxml2::XMLElement* node);

		public:
			XmlConfig() {}

			virtual void loadConfig(const std::string& path) override;
	};
}


#endif 
