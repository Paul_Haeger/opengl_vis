/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __INPUT_BASE_H__
#define __INPUT_BASE_H__
#include "tinyxml2.h"
#include "config/configexception.h"
#include <utility>

/** \brief Contains all the implementations necessary to read out and store the configuration. 
 **/
namespace config 
{
		class Config;

		/** \brief Defines a resolution type. First is width, second is height.
		 **/
		typedef std::pair<unsigned int, unsigned int> Resolution_t;
		
	
		/**
		 * Defines the type of the input.
		 **/
		enum InputType {
		//	Default,
			Image,
			ImageSequence,
			Audio,
			FrameBuffer,
			Unitialized
		};

		/** If input type is default, then what exactly we are using
		 **/
		enum DefaultType {
			Time,
			Frame,
			Framerate,
			Resolution,
			SamplingRate,
			Duration,
			FFFTrail,
			None		
		};

		/**
		 * Base class for all input types.
		**/
		class Input { //TODO: Refactor names
			protected:
				InputType in = Unitialized;
				std::string internalName;
				Config* parent;
			public:
				Input() = delete;

				Input(Config* configParent) : parent(configParent) { }
				
				virtual ~Input() {}

				/**
				 * \brief Returns the type of the current class. 
				 * You need to set in variable in constructor if you derive from this.
				 * \return The Inputtype
				 **/
				inline InputType getType() const {return in;}

				/**
				 * Returns the internal name. This matches the id specified in the configuration file.
				 **/
				inline std::string getInternalName() const {return internalName;}

				/** \brief In this you implement the parsing of the configuration.This needs to be implemented.
				 *  You get an element with its sub elements. You dont have to worry about the wrong input type being given.
				 * If anything is wrong you should throw a ConfigException.
				 * \param[in] c The XML element to parse.
				 * \see ConfigException
				 **/
				virtual void initFromXml(const tinyxml2::XMLElement* c) = 0;
		};	
		
		
		/**
		 * \brief Converts the text value of the node to an int.
		 * Converts the text value of the node. Throws a ConfigException if conversion fails.
		 * \param[in] node The node where we want to parse the text from.
		 *
		 * \return The parsed int.
		 * \throw ConfigExcpetion
		 * \pre <tt>node != NULL</tt>
		 */
		int nodetoi(const tinyxml2::XMLElement* node);

		/**
		 * \brief Converts the text value of the node to an usigned int. 
		 * Throws a ConfigExcpetion on failure.
		 * \param[in] node  The node where we want to parse the text from.
		 *
		 * \return The parsed unsigned int.
		 * \throw ConfigException
		 * \pre <tt>node != NULL</tt>
		 */
		unsigned int nodetoui(const tinyxml2::XMLElement* node);

		
		/**
		 * \brief Converts the text value of the node to a double value.
		 * Throws a ConfigurationException on failure.
		 * \param[in] node The node where we want to parse the text from. 
		 *
		 * \return The parsed double.
		 * \throw ConfigException
		 * \pre <tt>node != NULL</tt>
		 */
		double nodetod(const tinyxml2::XMLElement* node);

		/**
		 * \brief Converts the text value of the node to a float value.
		 * Throws a ConfigurationException on failure.
		 * \param[in] node The node where we want to parse the text from. 
		 *
		 * \return The parsed float.
		 * \throw ConfigException
		 * \pre <tt>node != NULL</tt>
		 */
		float nodetof(const tinyxml2::XMLElement* node);


		/**
		 * \brief Parses the resolution from the Xml node. Either in px or in % of parent.
		 *
		 * Plain numbers will be intepreted as px. Numbers ending with px will be intepreted as px.
		 * Numbers with % will be interpreted as a relative value given in percent. For this to work a parent resolution must be defined.
		 * \note The numbers must be positive integers.
		 *
		 * <code>123</code> will be interpreted the same as <code>123px</code>. The intepreted value is 123 pixels.
		 * <code>10%</code> will be interpreted as 10% of the the parent resolution.
		 * If a relative value is given without a parent resolution, then a ConfigException will be thrown. If parsing fails this will happen too. 
		 *
		 * \param[in] node  The node we want to parse from.
		 * \param[in] parentResolution The parent Resolution. A value of (0,0) is intepreted as an empty argument.
		 *
		 * \return The read resolution
		 * \throw ConfigException 
		 * \pre <tt>node != NULL</tt>
		 */
		Resolution_t parseResolution(const tinyxml2::XMLElement* node,const Resolution_t& parentResolution = Resolution_t(0,0));

		/**
		 * \brief Converts the char sequence to a lowercase string using the current c locale.
		 * \param[in] str  String to convert.
		 * \param[in] length  Optional length if string is not null terminated. With legth = 0 a null terminated string is assumed.
		 *
		 * \return The lowercase string.
		 * \pre <tt>str != NULL</tt>
		 */
		std::string toLower(const char* str, const size_t length = 0);

		/**
		 * \brief Converts the string to a lowercase string.
		 * \param[in] str  String to convert.
		 *
		 * \return The lowercase string.
		 */
		std::string toLower(const std::string& str);

} // namespace config





#endif 
