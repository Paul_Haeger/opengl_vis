/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __CONFIG__SHADER__PROGRAM_H__
#define __CONFIG__SHADER__PROGRAM_H__
#include <string>
#include <tinyxml2.h>
#include "config/inputbase.h"
#include <vector>
#include "config/configexception.h"


namespace config
{

//	class Config;	

	struct UniformData{
		
		UniformData(bool inIsCustom, const std::string& inInternalName, config::DefaultType inType, const std::string& inId)
		: isCustom(inIsCustom), internalName(inInternalName), type(inType), id(inId){}
		bool isCustom = false;
		std::string internalName;
		
		config::DefaultType type; //If type is default the exact input is defined here
		std::string id; //If type is custom, then id is used
	};	

/** \brief This class keeps all the data for constructing a shaderprogram.
 *
 * This should not be confused with the gl::ShaderProgram class, this is for data only.
 **/
class ShaderProgram {
	//Config* parent = nullptr; unused
	std::string fragmentPath;
	std::vector<UniformData> uniforms;
	std::string outputId; //If specified, output will be written to this framebuffer
public:
	//ShaderProgram(Config* inParent);

	void initFromXml(tinyxml2::XMLElement* node);

	//inline Config* getParent() const {return parent;}
	
	inline std::string getFragmentPath() const {return fragmentPath;}

	inline std::vector<UniformData>& getUniforms() {return uniforms;}

	inline const std::vector<UniformData>& getUniformsConst() const { return uniforms;}
};

} // namespace config
#endif 
