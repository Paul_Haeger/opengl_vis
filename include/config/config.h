/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __CONFIG__CONFIG_H__
#define __CONFIG__CONFIG_H__

#include "config/inputbase.h"
#include <map>
#include <memory>

namespace config{
	

	/** \brief Abstract base class for all config types.
	 * This class defines the basic options and methods for loading data.
	 * Most of the getter will be already be implemented here. The setting of the correct
	 * values is the job of the deriving class. This class was added so the 
	 * process of adding new configuration formats is easier(might be useful for a gui)
	 **/
	class Config
	{
	protected:

			std::map<std::string, std::shared_ptr<config::Input>> inputs;
			
			config::Resolution_t resolution; //Resolution of the output, first is width, second is height
			unsigned int frameDuration; //Length of sequence to render in frames
			std::string outputDir;
			std::string outputName;
			unsigned int frameRate;
			unsigned int samplingRate;
			unsigned int fftTrail;
	public:

			virtual void loadConfig(const std::string& path) = 0;

			inline config::Resolution_t getResolution() const {return resolution;}

			inline unsigned int getFrameDuration() const {return frameDuration;}

			inline unsigned int getFrameRate() const {return frameRate;}

			inline unsigned int getSamplingRate() const {return samplingRate;}

			inline unsigned int getFftTrail() const {return fftTrail;}

			inline const std::map<std::string, std::shared_ptr<config::Input>>& getInputs() const { return inputs;}  
	};

}

#endif
