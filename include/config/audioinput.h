/*
	OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __AUDIO_INPUT_H__
#define __AUDIO_INPUT_H__ 
#include "config/inputbase.h"
#include "config/configexception.h"
#include <string>

//class XmlConfig;

namespace config
{
		

class AudioInput : public Input {
	std::string path;
	/**How long should the song be playing**/
	float duration = 0.0f;
	/**After how many secodns should the audio start playing**/
	float offset = 0.0f; 
	/**How many seconds of the sound should be skipped before reading starts**/
	float skip = 0.0f;
	bool bIsLooped = false;  	
	unsigned int windowSize = 1024;
	public:
		AudioInput(Config* parent): Input(parent){ in = InputType::Audio; }
		virtual void initFromXml(const tinyxml2::XMLElement* node) override;	
		
		inline std::string getPath() const {return path;}
		inline float getDuration() const {return duration;}
		inline float getOffset() const {return offset;}
		inline float getSkip() const  {return skip;}
		inline bool isLooped() const {return bIsLooped;}
		inline unsigned int getWindowSize() const {return windowSize;}

		//Not directly done in the header because I dont want to include the xmlconfig class in the header.
		unsigned int getSamplingRate() const;
		unsigned int getFttTrail() const;
		unsigned int getFrameRate() const;
};


} // namespace config
#endif 
