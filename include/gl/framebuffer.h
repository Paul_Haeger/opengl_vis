/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __FRAME_BUFFER_H__
#define __FRAME_BUFFER_H__
#include "gl/glheader.h"
#include "gl/texture.h"
namespace gl
{

/** \brief Represents an OpenGL framebuffer.
 * The methods are made to closely match these of opengl, but also makes use 
 * of the RAII concepts of C++ that allow automatic deletion of the buffer.
 **/
class FrameBuffer {
public:
	enum Target : GLenum{
		FrameBufferBoth = GL_FRAMEBUFFER,
		FrameBufferRead = GL_READ_FRAMEBUFFER,
		FrameBufferDraw = GL_DRAW_FRAMEBUFFER
	};

private:
	GLuint buffer;

public:

	FrameBuffer();
	~FrameBuffer();
	
	/**
	 * \brief Binds the current frame buffer agains the target.
	 *
	 * \param bufferTarget The buffer target the buffer should be bound against. Both read and write by default.
	 */
	void bind(Target bufferTarget = Target::FrameBufferBoth) const;

	/**
	 * \brief Returns the gluint of the internal buffer.
	 *
	 * \return The gluint of the internal buffer.
	 */
	GLuint getInternalBuffer() const;

	/**\brief Close mapping of the glFramebufferTexture2D function. 
	 *
	 * Check <a href="https://www.khronos.org/registry/OpenGL-Refpages/es2.0/xhtml/glFramebufferTexture2D.xml">Khronos documentation</a> for more information. 
	 *
	 * \param[in] target The framebuffertarget we want to attach to.
	 * \param[in] attachment The type of attachment that should be made on the buffer.
	 * \param[in] targetType The type of the texture target we want to attach.
	 * \param[in] texTarget The texture we want to bind to.
	 * \param[in] level The mipmap level. According to the documentation it must be 0, so there is a default value for convenience.
	 **/
	static void FramebufferTexture2D(Target target, GLenum attachment, gl::Texture::Type targetType, gl::Texture texTarget, GLint level = 0);

};
}
#endif 
