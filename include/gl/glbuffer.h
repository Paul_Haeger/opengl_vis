/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __GL_BUFFER_H__
#define __GL_BUFFER_H__
#include "gl/glheader.h"
#include <vector>

namespace gl
{

/** \brief Represents one or more OpenGL buffer objects.
 *
 * Currently only a Vertex Buffer (alias Array Buffer) and Index Buffer (alias Element Buffer) are implemented.
 * For more information look <a href="https://www.khronos.org/opengl/wiki/Buffer_Object">here</a>.
 **/
class GLBuffer {
public:
 
/** \brief The possible buffer types.
 *
 * Only the VertexBuffer (Array Buffer) amd IndexBuffer (Element Buffer) is implemented.
 * The values of the enum are identical to the OpenGL values.
 * For more information on the enums look <a href="https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBindBuffer.xhtml">here</a>.
 **/
enum Type : GLenum {
	VertexBuffer = GL_ARRAY_BUFFER,
	IndexBuffer = GL_ELEMENT_ARRAY_BUFFER
};

/** \brief The different draw usages.
 *
 * This enum implements some of the draw usages.
 * The values are identical to the OpenGL values.
 * For more information look <a href="https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBufferData.xhtml">here</a>.
 **/
enum Usage: GLenum {
	StreamDraw = GL_STREAM_DRAW,
	StreamRead = GL_STREAM_READ,
	StreamCopy = GL_STREAM_COPY,
 	StaticDraw = GL_STATIC_DRAW,
	StaticRead = GL_STATIC_READ,
	StaticCopy = GL_STATIC_COPY,
	DynamicDraw = GL_DYNAMIC_DRAW,
	DynamicRead = GL_DYNAMIC_READ,
	DynamicCopy = GL_DYNAMIC_COPY
};

private:

GLuint * buffers = nullptr;
GLsizei bufferCount = 0;
Type bufferType;

public:

/** \brief Creates num buffers of the specified type. 
 *
 * \param type The type of buffer we want to create.
 * \param num The number of buffers to create.
 **/
GLBuffer(Type type, GLsizei num = 1);

~GLBuffer();

/** \brief Binds the specified buffer.
 *
 * If only one buffer was created, then index must be 0.
 * If more than one buffer was created you can address the buffer over the zero based index.
 * \param index The index of the buffer to bind.
 **/
void bindBuffer(GLsizei index = 0);

/** \brief Gets the underlying buffer object.
 *
 * If only one buffer was created, then the index must be 0.
 * If more than one buffer was created you can address it over the zero based index. 
 * 
 * \param index The index of the buffer to get.
 *
 * \return The underlying GLuint of the buffer object.
 **/
GLuint getBuffer(GLsizei index = 0);

/** 
 * \brief Writes data to the currently bound buffer target.
 *
 * Made a template method for convenience. For more information look. 
 * <a href="https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBufferData.xhtml">here</a>.
 *
 * \param target Which buffer type we should write.
 * \param size The number of elements in the array.
 * \param bufferData A pointer to the data.
 * \param usage Usage of the buffer.
 **/
template<typename T>
static void bufferData(Type target,  GLsizeiptr size, const T* bufferData, Usage usage){
	glBufferData(target, sizeof(T) * size, (const void*)(bufferData), usage);
}

};

}
#endif 
