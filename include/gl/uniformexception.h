/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __UNIFORM_EXCEPTION_H__
#define __UNIFORM_EXCEPTION_H__ 

#include <stdexcept>

namespace gl 
{
		

class UniformException : public std::runtime_error{
public:
		UniformException(const char* what_arg) 
				: std::runtime_error(what_arg) {}
		UniformException(const std::string& what_arg)
				: std::runtime_error(what_arg) {}
};
}
#endif 
