/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __SHADER_PROGRAM_H__
#define __SHADER_PROGRAM_H__
#include "gl/glheader.h"
#include "gl/shader.h"

namespace gl{
	


/** Creates a new shader program. The program will be deleted on deconstruction.
 * **/
class ShaderProgram{
	GLuint program;
	
public:
	/** \brief Creates a new shader program.
	 **/
	ShaderProgram();

	/** \brief Deletes the shader programm
	 **/
	~ShaderProgram();

	/** \brief Attaches the specified shader.
	 * Throws a ShaderException on error.
	 *
	 * \param[in] s The shader to be linked.
	 *
	 * \throw ShaderException
	 **/
	void attachShader(const Shader& s);	

	/** \brief Links the attached shaderst to the program. Shaders can be freed after that.
	 * Throws a ShaderException on error.
	**/
	void linkProgram();

	/** \brief Sets the current program to be used for rendering.
	 **/
	void useProgram() const;

	/**
	 * \brief Gets the location of the uniform. Returns -1 if error or nothing found.
	**/
	GLint getUniformLocation(const std::string& val) const;


};
}
#endif 
