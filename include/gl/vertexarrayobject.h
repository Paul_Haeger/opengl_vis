/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __VERTEX_ARRAY_OBJECT_H__
#define __VERTEX_ARRAY_OBJECT_H__
#include "gl/glheader.h"
#include <set>

namespace gl {
		

/** Wrapper for the Vertex Array Object
 **/
class VertexArrayObject{

		GLuint vao;

public:
		VertexArrayObject();
		~VertexArrayObject();

		void bind() const;

		static void vertexAttribPointer(GLuint index, GLint size, GLenum type,	GLboolean normalized,  	GLsizei stride, const void * pointer);

		static void enableVertexAttribArray(GLuint index);

		static void disableVertexAttribArray(GLuint index);

};
}
#endif //__VERTEX_ARRAY_OBJECT_H__
