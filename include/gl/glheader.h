/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __GL_HEADER_H__
#define __GL_HEADER_H__ 

/** \namespace gl
 * \brief All the relevant gl implementations wrapped as a class are contained here.
 **/

#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#endif //__GL_HEADER_H__
