/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __SHADER_H__
#define __SHADER_H__
#include "gl/glheader.h"
#include <string>

namespace gl{

/** \brief This class wraps an OpenGL shader object.
 *
 * This class keeps the code and the shader object internally. This class
 * will automatically destroy the shader when its deconstructed.
 * When code is loaded the shader will be created. There is no shader created by default.
 * If there is any error there will be a ShaderException thrown.
 * This object can be freed after it was sucessfully compiled and linked to a program. 
 * In the moment only shaders of the type Vertex and Fragment-shader are supported.
 *
 * \See gl::ShaderProgram ShaderException
 **/
class Shader {
public:
	enum Type : GLenum {
		Vertex = GL_VERTEX_SHADER,
		Fragment = GL_FRAGMENT_SHADER
	};

private:
	std::string shaderCode;
	GLuint  shader;
	bool bIsCreated = false;
	bool bIsCompiled = false;
	

public:

	Shader();

	~Shader();

	/**
	 * \brief Creates a shader of the type and loads the code.
	 * 
	 * This constructor creates an internal shader object of the type and loads the code.
	 * The code has to be compiled separately.
	 *
	 * \param[in] code  The code to load.
	 * \param[im[ type  The type of shader to be created.
	 */
	Shader(const std::string& code, Type type);

	/**
	 * \brief Creates a shader of the type and stores the code.
	 * 
	 * This constructor creates an internal shader object of the type and loads the code.
	 * The code has to be compiled separately.
	 *
	 * \param[in] code  The code to load.
	 * \param[im[ type  The type of shader to be created.
	 *
	 * \pre <tt>code != NULL</tt>
	 */
	Shader(const char* code, Type type);

	/**
	 * \brief Creates the shader and stores the code in the string. The shader is not compiled.
	 * An internal OpenGL shader object will be created. The code that is passed will be stored
	 * in a variable. The code will only be uploaded to the object when its compilation is attempted.
     * This method does not compile the shader. If creation fails a ShaderException will be thrown.
	 *
	 * \param[in] code  The String that contains the code.
	 * \param[in] type  The type of shader to be loaded.
	 *
	 * \throw ShaderException
	 */
	void fromCode(const std::string& code, Type type);


	/**
	 * \brief Creates the shader and stores the source code specified in the file.
	 * This method tries to load the shader code from the file. A ShaderException is
	 * thrown on failure to load the file or when the shader creation failed.
	 * 
	 * \param[in] path  The path to the shader code file.
	 * \param[in] type  The type of shader to be loaded.
	 *
	 * \throw ShaderException
	 */
	void fromFile(const std::string& path, Type type);

	/**
	 * \brief Compiles the shader code.
	 * This method loads and compiles the shader code that wais stored. 
	 * If this method is called before a shader is created, then
	 * a ShaderException will be thrown. If the shader fails to 
	 * compile a ShaderException with the glGetShaderInfoLog message will
	 * be thrown.
	 *
	 * \throw ShaderException
	 */
	void compile();

	/**
	 * \brief Frees the internal data.
	 *
	 * This methods frees the internal data. The code ad the internal shader object 
	 * get deleted. This method can be called once the shader was succesfully linked to
	 * a programm. If no shader is created this method will do nothing. 
	 *
	 * \see gl::ShaderProgramm
	 */
	void free() noexcept;

	/**
	 * \brief Wether the shader was sucessfully created or not.
	 *
	 * \return True if the shader was sucessfully created, false if not.
	 */
	inline bool isCreated() const { return bIsCreated;}
	
	/**
	 * \brief Whether the shader was sucessfully compiled or not.
	 * 
	 * \return True if the shader was sucessfully compiled, false if not or if no compilation has been attempted.
	 */
	inline bool isCompiled() const { return bIsCompiled;}

	/**
	 * \brief Gets the internal shader object.
	 *
	 * \return The internal shader object.
	 */
	inline GLuint getShaderObject() const {return shader;}
};





}
#endif //__SHADER_H__
