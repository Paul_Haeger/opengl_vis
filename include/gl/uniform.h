/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __UNIFORM_H__
#define __UNIFORM_H__

#include "gl/glheader.h"
#include <string>
#include <glm/gtc/type_ptr.hpp>
#include <type_traits>
#include "gl/uniformexception.h"

namespace gl
{
		


class ShaderProgram;



/**
 * \brief A class that contains the information needed for an uniform.
 * This class keeps track of the location and also allows for easy value writing,
 * as it makes use of c++ function overloading. 
 */
class Uniform {
	GLint location;



public:
	Uniform() = delete;
	/** Gets the uniform of the program.
	 * Throws a UniformException if the uniform doesnt exists.
	 **/
	Uniform(const ShaderProgram& p,const std::string& name);

	Uniform(const Uniform& rhs);

	Uniform& operator=(const Uniform& rhs);

//Intentionally not define to avoid usage of unsupportet types

	template <typename T>
	inline void setValue(T v1);

	template <typename T>
	inline void setValue(T v1, T v2);

	template <typename T>
	inline void setValue(T v1, T v2, T v3);

	template <typename T>
	inline void setValue(T v1, T v2, T v3, T v4);
	
	//inline void setValue(const glm::fvec2&	v) {	glUniform2fv(location, 1,glm::value_ptr(v));	}

//	inline void setValue(const glm::fvec3& v)  {
//		glUniform3fv(location,1, glm::value_ptr(v));
//	}

//	inline void setValue(const glm::fvec4& v) {
//		glUniform4fv(location, 1, glm::value_ptr(v));
	



};
	//Expanded as needed
	//---------------------------Float-----------------------------------------------
	template <>
	inline void Uniform::setValue<GLfloat>(GLfloat v1) {glUniform1f(location,v1);} 

	
	template <>
	inline void Uniform::setValue<GLfloat>(GLfloat v1, GLfloat v2) {glUniform2f(location,v1,v2);}


	template<>
	inline void Uniform::setValue<GLfloat>(GLfloat v1, GLfloat v2, GLfloat v3){
		glUniform3f(location,v1,v2,v3);
	}

	template <>
	inline void Uniform::setValue<GLfloat>(GLfloat v1, GLfloat v2, GLfloat v3, GLfloat v4){
		glUniform4f(location, v1, v2, v3, v4);
	}

//-----------------Int----------------------
	template<>
	inline void Uniform::setValue<GLint>(GLint v1){
		glUniform1i(location, v1);
	}
//---------------Unsigned Int---------------
	
	template <>
	inline void Uniform::setValue<GLuint>(GLuint v1){
		glUniform1ui(location, v1);
	}

}
#endif
