/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __TEXTURE_H__
#define __TEXTURE_H__
#include "gl/glheader.h"
#include <stdexcept>

#include <bitset>

namespace gl
{
		
/** \brief This class represents an OpenGL texture.
 *
 *	The class keeps all the data necessary for managing a texture. If no target is given in the constructor,
 *	the texture will automatically  target a Texture2D target. The class keeps internally track of all the
 *	activated textures to avoid any id clashes. Many methods are static in order to keep the binding close to 
 *	the OpenGL api. This is done so the user has to bind what he wants to use first, and there are also no binding 
 *	side effects which could create behaviour that is painful to debug.
 **/
class Texture {
	public:
			enum Wrapping : GLint {
					Repeat = GL_REPEAT,
					MirroredRepeat = GL_MIRRORED_REPEAT,
					ClampToEdge = GL_CLAMP_TO_EDGE,
					ClampToBorder = GL_CLAMP_TO_BORDER
			};

			enum Filtering : GLint {
					Linear = GL_LINEAR,
					Nearest = GL_NEAREST
			};

			enum Type : GLenum {
					Texture1D = GL_TEXTURE_1D,
					Texture2D = GL_TEXTURE_2D,
					Texture3D = GL_TEXTURE_3D
			};

	private:
			GLuint texture;
			Type type;
			Wrapping wrap = Repeat;
			Filtering filtering = Nearest;

			//keeps track of the activated textures
			static std::bitset<GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS - 1> isActivated; 

	public:
			Texture(Type textureType = Texture2D);
			~Texture();

			void bind() const;
			inline void setWrap(Wrapping w){ 	wrap = w;}//moved here because of weird compile error

			inline void setFiltering(Filtering f){filtering = f;} // ""
			inline Type getType() const {return type;}
			
			

			/**
			 * \brief Static Method. Activates the <b>currently bound texture at the specified location.
			 * Activates the currently bound texture at the specified location. The location is offset by the macro so there is no need to use it. If the number is too large a
			 * std::out_of_range exception will be thrown.
			 * \param[in] unit  The unit location to place the texture. It is automatically offset so no need to use the macro
			 *
			 * \throw std::out_of_range
			 */
			static void ActiveTexture(GLenum unit = 0);
	
			/**
			 * \brief Static Method. Close mapping of the OpenGL function.
			 *
			 * For more information see <a href="https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glTexImage2D.xhtml">here</a>.
			 * \param[in] target  The target texture.
			 * \param[in] level  Level of detail.
			 * \param[in] internalFormat Number of color components.
			 * \param[in] width  Texture width.
			 * \param[in] height  Texture height.
			 * \param[in] border  Must be 0.
			 * \param[in] format  Format of the pixel data.
			 * \param[in] type  Data type of the pixel data.
			 * \param[in] data  The pointer to the image data in memory. If null data will be allocated but not filled.
			 *
			 */
			static void TexImage2D(Type target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* data);

			/**
			 * \brief Static Method. Clears the internal list that keeps track of the activated textures. 
			 *
			 * Do this before you draw a new program.
			 */
			static void ClearActivationSet();

			/**
			 * \brief Static Method. Activates the currently <b>bound texture</b> at the next free unit and returns the position. 
			 *
			 * Throws an std::out_of_range if all textureunits are taken(very unlikely on most systems).
			 * \return The position
			 * \throw std::out_of_range 
			 */
			static GLenum ActiveTextureAtNextFreeUnit();

			/**
			 * Generates Mipmaps for the currently bound texture.
			 * \param[in] target  Texture target.
			 */
			static void GenerateMipmaps(Type target);

			
			/**
			 * \brief Returns the internal texture object.
			 *
			 * \return The internal texture object.
			 */
			inline GLuint getInternalTexture() const {
					return texture;
			}

};

} // namespace gl
#endif 
