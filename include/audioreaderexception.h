/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**@file	/home/paul/projects/opengl-vis/include/audioreaderexception.h
 * @author	paul
 * @version	802
 * @date
 * 	Created:	20th Aug 2020
 * 	Last Update:	20th Aug 2020
 */

#ifndef __AUDIOREADEREXCEPTION_H__
#define __AUDIOREADEREXCEPTION_H__

#include <stdexcept>
#include <string>
class AudioReaderException : std::runtime_error {
public:
	AudioReaderException(const char* msg) 
			: std::runtime_error(msg) 
			{}
	AudioReaderException(const std::string& msg) 
			: std::runtime_error(msg) 
			{}
};

#endif//  __AUDIOREADEREXCEPTION_H__
