/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include "audioreader.h"
#include <cmath>
#define EPSILON 0.00001

TEST(AudioReaderTest, NormalReadTest){
	AudioReader r("test/1024_samples_44100.wav", 44100);
	auto data = r.getSamples(2., 1024);
	//check if there are any prepended or appended 0s
	ASSERT_GT(fabs(data.front()), EPSILON);
	ASSERT_GT(fabs(data.back()), EPSILON);
}

TEST(AudioReaderTest, PrependTest){

	AudioReader r("test/2048_samples_44100.wav", 44100);
	auto data = r.getSamples(.0116, 1024); //read not enough for the buffer. We will be provided with 512 samples
	//check if there are any prepended or appended 0s
	ASSERT_LT(fabs(data.front()), EPSILON);
	ASSERT_LT(fabs(data[511]), EPSILON);
	ASSERT_GT(fabs(data[512]), EPSILON); //We  should have samples now
}
