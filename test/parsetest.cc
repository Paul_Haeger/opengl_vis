/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include "config/inputbase.h"
#include "tinyxml2.h"
#include <limits>
#include <string>

TEST(ParseTest, nodetoi){
		tinyxml2::XMLDocument doc;
		auto node = doc.NewElement("value");
		node->InsertNewText("4");
		ASSERT_EQ(config::nodetoi(node), 4);
		

		std::string tooLarge = std::to_string(std::numeric_limits<int>::max()) + "000000000";
		
		node = doc.NewElement("value2");
		node->InsertNewText(tooLarge.c_str());	
//		std::cout <<  "Number is: " << tooLarge;	

		ASSERT_THROW(config::nodetoi(node),config::ConfigException);
		
		node = doc.NewElement("value3");
		node->InsertNewText("a");

		ASSERT_THROW(config::nodetoi(node), config::ConfigException);
}

TEST(ParseTest, nodetoui){
		tinyxml2::XMLDocument doc;
		auto node = doc.NewElement("value");
		node->InsertNewText("5135");
		ASSERT_EQ(config::nodetoui(node), 5135);

		node = doc.NewElement("value2");
		node->InsertNewText("-1");
		ASSERT_THROW(config::nodetoui(node), config::ConfigException);

		node = doc.NewElement("value3");
		std::string tooLarge = std::to_string(std::numeric_limits<unsigned int>::max()) + "0";
		node->InsertNewText(tooLarge.c_str());
		ASSERT_THROW(config::nodetoui(node), config::ConfigException);

		node = doc.NewElement("value4");
		node->InsertNewText("adas");
		ASSERT_THROW(config::nodetoui(node), config::ConfigException);
}

TEST(ParseTest, nodetof){
	tinyxml2::XMLDocument doc;
	auto node = doc.NewElement("value");
	node->InsertNewText("1.0");
	ASSERT_FLOAT_EQ(config::nodetof(node), 1.0f);

	node = doc.NewElement("value2");
	node->InsertNewText("2");
	ASSERT_FLOAT_EQ(config::nodetof(node), 2.0f);

	node = doc.NewElement("value3");
	node->InsertNewText("1e2");
	ASSERT_FLOAT_EQ(config::nodetof(node), 100.0f);

	node = doc.NewElement("value4");
	node->InsertNewText("a");
	ASSERT_THROW(config::nodetof(node), config::ConfigException);

	node = doc.NewElement("value5");
	std::string tooLarge = "1" + std::to_string(std::numeric_limits<float>::max());
//	std::cout << "Large value: " << tooLarge << std::endl;
	node->InsertNewText(tooLarge.c_str());
	ASSERT_THROW(config::nodetof(node), config::ConfigException);

}

TEST(ParseTest, nodetod){
	tinyxml2::XMLDocument doc;
	auto node = doc.NewElement("value");
	node->InsertNewText("1.0");
	ASSERT_DOUBLE_EQ(config::nodetod(node), 1.0);

	node = doc.NewElement("value2");
	node->InsertNewText("2");
	ASSERT_DOUBLE_EQ(config::nodetod(node), 2.0);

	node = doc.NewElement("value3");
	node->InsertNewText("1e2");
	ASSERT_DOUBLE_EQ(config::nodetod(node), 100.0f);

	node = doc.NewElement("value4");
	node->InsertNewText("a");
	ASSERT_THROW(config::nodetod(node), config::ConfigException);

	node = doc.NewElement("value5");
	std::string tooLarge = "1" + std::to_string(std::numeric_limits<double>::max());
//	std::cout << "Large value: " << tooLarge << std::endl;
	node->InsertNewText(tooLarge.c_str());
	ASSERT_THROW(config::nodetod(node), config::ConfigException);
}


TEST(ParseTest, parseResolution){
  tinyxml2::XMLDocument doc;
  auto node = doc.NewElement("resolution");
  node->InsertNewChildElement("width")->InsertNewText("100");
  node->InsertNewChildElement("height")->InsertNewText("200");
	
  ASSERT_EQ(config::parseResolution(node), config::Resolution_t(100,200));

  node = doc.NewElement("resolution");
  node->InsertNewChildElement("width")->InsertNewText("100px");
  node->InsertNewChildElement("height")->InsertNewText("200px");
	
  ASSERT_EQ(config::parseResolution(node), config::Resolution_t(100,200));
  node = doc.NewElement("resolution");
  node->InsertNewChildElement("width")->InsertNewText("100%");
  node->InsertNewChildElement("height")->InsertNewText("200%");

  ASSERT_EQ(config::parseResolution(node, config::Resolution_t(100, 200)), config::Resolution_t(100,400));

  //TODO: Implement if exceptions are thrown. Many cases to check....
  
}
