/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "testwindow.h"
//Quick macro to reduce some writing work. Generates a key value pair with the enum and the enum name.
#define K_V_Pair(X) {X, #X}

TestWindow::TestWindow()
{
	if (!glfwInit()) {
				throw std::runtime_error("Failed to initialize GLFW3");
			}
			glfwWindowHint(GLFW_CONTEXT_CREATION_API ,GLFW_NATIVE_CONTEXT_API);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);		
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
			glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

			window = glfwCreateWindow(300, 300, "Shader Test", NULL, NULL);
			if(!window){
				throw std::runtime_error("Failed to creat the window");
			}
			glfwMakeContextCurrent(window);
			glfwSetFramebufferSizeCallback(window, [](GLFWwindow* win, int width, int height){
						glViewport(0,0, width, height);
					}
			);

			if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
				throw std::runtime_error("Failed to load the ProcAddress");
			}
				
			glDebugMessageCallback(&TestWindow::debugMessageCallback, NULL);	
				
			glfwPollEvents();
}

/*virtual*/  TestWindow::~TestWindow()
{
	if(window){
		glfwDestroyWindow(window);
	}
}

 const std::map<GLenum, std::string> TestWindow::msgSource = {
				K_V_Pair(GL_DEBUG_SOURCE_APPLICATION),
				K_V_Pair(GL_DEBUG_SOURCE_APPLICATION),
				K_V_Pair(GL_DEBUG_SOURCE_APPLICATION),
				K_V_Pair(GL_DEBUG_SOURCE_THIRD_PARTY),
				K_V_Pair(GL_DEBUG_SOURCE_APPLICATION),
				K_V_Pair(GL_DEBUG_SOURCE_APPLICATION) 		
			};

 const std::map<GLenum, std::string> TestWindow::msgType = {
				K_V_Pair(GL_DEBUG_TYPE_ERROR),
				K_V_Pair(GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR),
				K_V_Pair(GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR),
				K_V_Pair(GL_DEBUG_TYPE_PORTABILITY),
				K_V_Pair(GL_DEBUG_TYPE_PERFORMANCE),
				K_V_Pair(GL_DEBUG_TYPE_PERFORMANCE), 		
				K_V_Pair(GL_DEBUG_TYPE_PUSH_GROUP), 		
				K_V_Pair(GL_DEBUG_TYPE_POP_GROUP), 		
				K_V_Pair(GL_DEBUG_TYPE_OTHER) 		
			};

 const std::map<GLenum, std::string> TestWindow::msgSeverity = {
				K_V_Pair(GL_DEBUG_TYPE_OTHER),
				K_V_Pair(GL_DEBUG_SEVERITY_MEDIUM),
				K_V_Pair(GL_DEBUG_SEVERITY_LOW),
				K_V_Pair(GL_DEBUG_SEVERITY_NOTIFICATION),
			};
