/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include <algorithm>
#include <gtest/gtest-param-test.h>
#include <memory>
#include <stdexcept>
#include "gl/shaderexception.h"
#include "gl/uniform.h"
#include "gl/uniformexception.h"
#include "testwindow.h"
#include "gl/glheader.h"
#include "gl/shader.h"
#include "gl/shaderprogram.h"
#include "render/shaderprogram.h"
#include "screenbuffer.h"
#include <iostream>




class ShaderTest : public ::testing::Test{
	public:

		
		std::unique_ptr<TestWindow> window;

		std::unique_ptr<ScreenBuffer> screenBuf;


		virtual void SetUp(){

			window = std::unique_ptr<TestWindow>(new TestWindow());
			
			screenBuf = std::unique_ptr<ScreenBuffer>(new ScreenBuffer());
			
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glClearColor(0.f,0.f, 0.f, 0.f);
			glfwSwapBuffers(window->getWindow());
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glClearColor(0.f,0.f, 0.f, 0.f);

		}	

		virtual void TearDown(){
			screenBuf.reset();
			window.reset();
		}
};

/**
* Test the vertex shader generation, test 1
**/
TEST_F (ShaderTest, VertexShaderGeneration1) { 
		std::string fragmentPath("test/shaders/shader.frag");
		gl::Shader fs;	
		fs.fromFile(fragmentPath, gl::Shader::Fragment);

		ASSERT_NO_THROW(fs.compile());
		ASSERT_NO_THROW(render::ShaderProgram::generateVertexShader(fragmentPath));

		std::string vertex = render::ShaderProgram::generateVertexShader(fragmentPath);
		gl::Shader vs(vertex, gl::Shader::Vertex);
		ASSERT_NO_THROW(vs.compile());
		
		screenBuf->bindVao();		

		gl::ShaderProgram p;
		p.attachShader(vs);
		p.attachShader(fs);
		ASSERT_NO_THROW(p.linkProgram());
			
		fs.free();
		vs.free();

		p.useProgram();
		
		screenBuf->bindBuffers();

		glClearColor(0.f,0.f, 0.f, 0.f);

		screenBuf->draw();
		glfwSwapBuffers(window->getWindow());
		glfwPollEvents();

		float val = 0.00f;

		glReadPixels(0,0, 1, 1, GL_GREEN, GL_FLOAT, &val);	
		ASSERT_FLOAT_EQ(val , 1.0f);

}

TEST_F (ShaderTest, FailedFileRead){
		gl::Shader errorShader;
		ASSERT_THROW(errorShader.fromFile("no file", gl::Shader::Vertex), gl::ShaderException);
}

TEST_F (ShaderTest, FailedCompile){
		gl::Shader errorShader;
		errorShader.fromCode("dasdasd", gl::Shader::Vertex);
		ASSERT_THROW(errorShader.compile(), gl::ShaderException);
}

TEST_F (ShaderTest, UnfiormTest){
	gl::Shader vert, frag;
	vert.fromFile("test/shaders/shader_with_uniform.vert",gl::Shader::Vertex);
	frag.fromFile("test/shaders/shader_with_uniform.frag",gl::Shader::Fragment);	

	vert.compile();
	frag.compile();

	gl::ShaderProgram program;
	program.attachShader(vert);
	program.attachShader(frag);

	program.linkProgram();
	vert.free();
	frag.free();

	ASSERT_NO_THROW(gl::Uniform(program, "green"));
	ASSERT_THROW(gl::Uniform(program, "unused"), gl::UniformException);
	ASSERT_THROW(gl::Uniform(program, "nonexistent"), gl::UniformException);
	gl::Uniform u(program, "green");

	screenBuf->bindBuffers();

	program.useProgram();
	u.setValue<GLfloat>(1.f);

	glClearColor(0.f,0.f, 0.f, 0.f);

	screenBuf->draw();
	glfwSwapBuffers(window->getWindow());
	glfwPollEvents();

	float val = 0.00f;

	glReadPixels(0,0, 1, 1, GL_GREEN, GL_FLOAT, &val);	
	ASSERT_FLOAT_EQ(val , 1.0f);

}

TEST_F (ShaderTest, MismatchedUniformTest){
	gl::Shader vert, frag;
	vert.fromFile("test/shaders/shader_with_uniform_missmatched.vert",gl::Shader::Vertex);
	frag.fromFile("test/shaders/shader_with_uniform.frag",gl::Shader::Fragment);	

	vert.compile();
	frag.compile();

	gl::ShaderProgram program;
	program.attachShader(vert);
	program.attachShader(frag);

	program.linkProgram();
	vert.free();
	frag.free();

	ASSERT_NO_THROW(gl::Uniform(program, "green"));
	ASSERT_THROW(gl::Uniform(program, "unused"), gl::UniformException);
	ASSERT_THROW(gl::Uniform(program, "nonexistent"), gl::UniformException);
	gl::Uniform u(program, "green");

	screenBuf->bindBuffers();

	program.useProgram();
	u.setValue<GLfloat>(1.f);

	glClearColor(0.f,0.f, 0.f, 0.f);

	screenBuf->draw();
	glfwSwapBuffers(window->getWindow());
	glfwPollEvents();

	float val = 0.00f;

	glReadPixels(0,0, 1, 1, GL_GREEN, GL_FLOAT, &val);	
	ASSERT_FLOAT_EQ(val , 1.0f);

}

TEST_F (ShaderTest, MismatchedVersionTest){
	gl::Shader vert, frag;
	vert.fromFile("test/shaders/shader_version_missmatched.vert",gl::Shader::Vertex);
	frag.fromFile("test/shaders/shader.frag",gl::Shader::Fragment);	

	vert.compile();
	frag.compile();

	gl::ShaderProgram program;
	program.attachShader(vert);
	program.attachShader(frag);

	program.linkProgram();
	vert.free();
	frag.free();

	screenBuf->bindBuffers();

	program.useProgram();

	glClearColor(0.f,0.f, 0.f, 0.f);

	screenBuf->draw();
	glfwSwapBuffers(window->getWindow());
	glfwPollEvents();

	float val = 0.00f;

	glReadPixels(0,0, 1, 1, GL_GREEN, GL_FLOAT, &val);	
	ASSERT_FLOAT_EQ(val , 1.0f);

}

TEST_F (ShaderTest, MismatchedUniformVertexGeneratedTest){
	gl::Shader vert, frag;
	std::string fragCode("test/shaders/shader_with_uniform.frag");
	std::string code = render::ShaderProgram::generateVertexShader(fragCode);
	vert.fromCode(code ,gl::Shader::Vertex);
	frag.fromFile(fragCode ,gl::Shader::Fragment);	

	ASSERT_NO_THROW(vert.compile());
	ASSERT_NO_THROW(frag.compile());

	gl::ShaderProgram program;
	program.attachShader(vert);
	program.attachShader(frag);

	program.linkProgram();
	vert.free();
	frag.free();

	ASSERT_NO_THROW(gl::Uniform(program, "green"));
	ASSERT_THROW(gl::Uniform(program, "unused"), gl::UniformException);
	ASSERT_THROW(gl::Uniform(program, "nonexistent"), gl::UniformException);
	gl::Uniform u(program, "green");

	screenBuf->bindBuffers();

	program.useProgram();
	u.setValue<GLfloat>(1.f);

	glClearColor(0.f,0.f, 0.f, 0.f);

	screenBuf->draw();
	glfwSwapBuffers(window->getWindow());
	glfwPollEvents();

	float val = 0.00f;

	glReadPixels(0,0, 1, 1, GL_GREEN, GL_FLOAT, &val);	
	ASSERT_FLOAT_EQ(val , 1.0f);

}
