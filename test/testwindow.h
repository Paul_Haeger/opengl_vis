/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __TEST_TEST_WINDOW_H__
#define __TEST_TEST_WINDOW_H__
#include "gl/glheader.h"
#include <map>
#include <string>
#include <iostream>


/** \brief Class that initializes a GLFW window with debug context.
 *
 **/
class TestWindow
{
		GLFWwindow* window = nullptr;
		
		static const std::map<GLenum, std::string> msgSource; 		
		static const std::map<GLenum, std::string> msgType; 		
		static const std::map<GLenum, std::string> msgSeverity; 		


public:
    /* Argument-less constructor */
        TestWindow();
    /* Copy constructor */
        TestWindow(const TestWindow & rhs) = delete;
    /* Copy operator */
        TestWindow& operator=(const TestWindow & rhs) = delete;
    /* Destructor */
        virtual ~TestWindow();

		static void debugMessageCallback(GLenum source, GLenum type, GLuint id,
					 GLenum severity, GLsizei length, const GLchar* message, const void* userParam){
				try{
					std::cout << msgSource.at(source);
				}catch(std::out_of_range& e){
					std::cout << "Unkown source 0x" << std::hex << source;
				}
				std::cout << "   ";

				try{
					std::cout << msgType.at(type);
				}catch(std::out_of_range& e){
					std::cout << "Unkown type 0x" << std::hex << type;
				}
				std::cout << "   " << std::hex << id << " ";
				
				try{
					std::cout << msgSeverity.at(severity);
				}catch(std::out_of_range& e){
					std::cout << "Unkown severity 0x" << std::hex << severity;
				}
				std::cout << " " << std::string(message, static_cast<size_t>(length)) << std::endl  << std::endl;
			
		}	

		GLFWwindow* getWindow() const {return window;}
};
#endif

