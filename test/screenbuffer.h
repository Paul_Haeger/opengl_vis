/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __TEST_SCREEN_BUFFER_H__
#define __TEST_SCREEN_BUFFER_H__

#include "gl/glheader.h"
#include "gl/glbuffer.h"
#include "gl/vertexarrayobject.h"

/** \brief Class that initializes the data to draw a whole screen.
 * This class is there for testing purposes. It allows the user to easily initialize and 
 * use a plane that covers the whole window.
 * **/
class ScreenBuffer
{

	gl::GLBuffer vbo;
	gl::GLBuffer ebo;
	gl::VertexArrayObject vao;

	const GLfloat screen[12] = { 1.f, 1.f, 0.f,   //top right
						     	 1.f,  -1.f, 0.f,   //bottom right          |     |
	          			     	 -1.f,  -1.f, 0.f,   //bottom left         |     |
	                         	 -1.0f,  1.0f, 0.f,
	}; //top left      0-----3

	const GLuint indices[6] = {
		0, 1, 3, 
		1, 2, 3, 
	};


public:
    /* Argument-less constructor */
        ScreenBuffer();
    /* Copy constructor */
        ScreenBuffer(const ScreenBuffer & rhs) = delete;
    /* Copy operator */
        ScreenBuffer& operator=(const ScreenBuffer & rhs) = delete;
    /* Destructor */
       ~ScreenBuffer();


	   /**
		* \brief Binds the buffers.
		* This method will bind all the buffers. This is necessary in order to draw.
		*/
	void bindVao();

	void bindBuffers();

	/**
	 * \brief Draws 2 Triangles covering the screen.
	 */
	void draw();


};
#endif 
