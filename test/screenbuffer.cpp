/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "screenbuffer.h"
#include "gl/glbuffer.h"
#include "gl/vertexarrayobject.h"


ScreenBuffer::ScreenBuffer()
 : vbo(gl::GLBuffer::VertexBuffer), ebo(gl::GLBuffer::IndexBuffer)
{
			vao.bind();
			vbo.bindBuffer();
			gl::GLBuffer::bufferData(gl::GLBuffer::VertexBuffer, sizeof(screen), screen, gl::GLBuffer::StaticDraw);
			ebo.bindBuffer();
			gl::GLBuffer::bufferData(gl::GLBuffer::IndexBuffer, sizeof(indices), indices, gl::GLBuffer::StaticDraw);

			gl::VertexArrayObject::vertexAttribPointer(0, 3, GL_FLOAT,
					 GL_FALSE, 3 * sizeof(float), 0);
			gl::VertexArrayObject::enableVertexAttribArray(0);
	
}

ScreenBuffer::~ScreenBuffer(){
	gl::VertexArrayObject::disableVertexAttribArray(0);	
}

void ScreenBuffer::bindVao() 
{
	gl::VertexArrayObject::enableVertexAttribArray(0);
	vao.bind();
}

void ScreenBuffer::bindBuffers() 
{
	ebo.bindBuffer();
	vbo.bindBuffer();
}

void ScreenBuffer::draw() {
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}
