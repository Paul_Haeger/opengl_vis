/*
 OpenGL_Vis
    Copyright (C) 2021  Paul Häger <paul@paulhaeger.xyz>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include "config/inputbase.h"
#include "config/audioinput.h"
#include "config/framebufferinput.h"
#include "config/imageinput.h"
#include "config/imagesequenceinput.h"
#include "tinyxml2.h"

TEST(InputTest, AudioInputTest){
	tinyxml2::XMLDocument doc;
	auto node = doc.NewElement("input");
	node->SetAttribute("type", "audio");
	node->SetAttribute("id", "abc");
	node->SetAttribute("looped", "true");
	node->InsertNewChildElement("path")->InsertNewText("test");
	node->InsertNewChildElement("duration")->InsertNewText("180");
	node->InsertNewChildElement("offset")->InsertNewText("0");
	node->InsertNewChildElement("skip")->InsertNewText("30");
	node->InsertNewChildElement("name")->InsertNewText("audio");

	config::AudioInput ai(nullptr);
	ai.initFromXml(node);

	ASSERT_EQ(ai.getType(), config::InputType::Audio);
	ASSERT_EQ(ai.getPath(),std::string("test"));
	ASSERT_EQ(ai.isLooped(), true);
	ASSERT_FLOAT_EQ(ai.getDuration(), 180.0f);
	ASSERT_FLOAT_EQ(ai.getOffset(), 0.0f);
	ASSERT_FLOAT_EQ(ai.getSkip(), 30.0f);
	ASSERT_EQ(ai.getInternalName(), "audio");
	

}

TEST(InputTest, ImageInputTest){
		tinyxml2::XMLDocument doc;
		auto node = doc.NewElement("input");
		node->SetAttribute("type","image");
		node->SetAttribute("id","abc");
		node->InsertNewChildElement("path")->InsertNewText("test");
		node->InsertNewChildElement("wrap")->InsertNewText("Clamp to Edge");
		node->InsertNewChildElement("filtering")->InsertNewText("Linear");
		node->InsertNewChildElement("name")->InsertNewText("Testname");

		config::ImageInput ii(nullptr);
		ii.initFromXml(node);
		
		
		ASSERT_EQ(ii.getType(), config::InputType::Image);
		ASSERT_EQ(ii.getFilering(), config::ImageInput::Filtering::Linear);
		ASSERT_EQ(ii.getWrap(), config::ImageInput::Wrap::ClampToEdge);
		ASSERT_EQ(ii.getInternalName(), std::string("Testname"));

}

TEST(InputTest, ImageSequenceInputTest){
		tinyxml2::XMLDocument doc;
		auto node = doc.NewElement("input");
		node->SetAttribute("type","imageSequence");
		node->SetAttribute("id","abc");
		node->SetAttribute("looped", "true");
		node->InsertNewChildElement("path")->InsertNewText("test");
		node->InsertNewChildElement("wrap")->InsertNewText("Clamp to Edge");
		node->InsertNewChildElement("filtering")->InsertNewText("Linear");
		node->InsertNewChildElement("name")->InsertNewText("Testname");
		node->InsertNewChildElement("start")->InsertNewText("0");
		node->InsertNewChildElement("end")->InsertNewText("100");
		node->InsertNewChildElement("frameRate")->InsertNewText("10");
		node->InsertNewChildElement("formatString")->InsertNewText("%05d");

		config::ImageSequenceInput isi(nullptr);
		isi.initFromXml(node);
		
		
		ASSERT_EQ(isi.getType(), config::InputType::ImageSequence);
		ASSERT_EQ(isi.isLooped(), true);
		ASSERT_EQ(isi.getFilering(), config::ImageInput::Filtering::Linear);
		ASSERT_EQ(isi.getWrap(), config::ImageInput::Wrap::ClampToEdge);
		ASSERT_EQ(isi.getInternalName(), std::string("Testname"));
		ASSERT_EQ(isi.getStart(), 0);
		ASSERT_EQ(isi.getEnd(), 100);
		ASSERT_EQ(isi.getFrameRate(), 10);

}
